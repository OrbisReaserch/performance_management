<?php 
error_reporting(0);
class Manager_dashboard_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
		
	}	
 	
	
	 public function get_employee_rating_details($user_id)
	{
		
		$query=$this->db->query('select userD.id,userD.user_id,userD.employee_id,userD.employee_name,userD.department_id,userD.designation_id,userD.reporting_manager_id,dept.department_name,des.designation_name from user_details userD join department dept join designation des where dept.id=userD.department_id and des.id=userD.designation_id and userD.reporting_manager_id='.$user_id.' order by userD.employee_name asc');
		
		return $query->result();
		
	}
	public function filled_form_details($user_id)
	{
		// echo $user_id;exit;
		$query = $this->db->query('select r.id,r.employee_user_id,r.manager_user_id,r.question_id,r.rating_by_employee,r.rating_by_manager,r.total_score_by_employee,r.comment_by_employee,r.comment_by_manager,r.extra_achivement_by_employee,r.extra_achivement_by_manager,r.shortfall_in_performance_reason,r.last_updated_on,r.status,q.question from rating_form r INNER JOIN questions q where q.id=r.question_id and employee_user_id='.$user_id);
		return $query->result();
	}
	public function employee_details($user_id)
	{
		$query = $this->db->query('select userD.employee_name,userD.department_id,userD.designation_id,dept.department_name,des.designation_name from user_details userD INNER JOIN department dept JOIN designation des where dept.id=userD.department_id and des.id=userD.designation_id and user_id='.$user_id);
		return $query->result();
	}
	public function filled_form_details_by_manager($user_id)
	{
		$qry=$this->db->query('select reporting_manager_id from user_details where user_id='.$user_id);
		$rp_detail=$qry->result();
		// print_r($qry->result());exit;
		// $user_id= $this->session->userdata('id');
		 $manager_user_id= $rp_detail[0]->reporting_manager_id;
		 // echo 'select r.id,r.employee_user_id,r.manager_user_id,r.question_id,r.rating_by_manager,r.comment_by_manager,r.extra_achivement_by_manager,r.shortfall_in_performance_reason,r.last_updated_on,r.status,q.question from rating_form r INNER JOIN questions q where q.id=r.question_id  and r.manager_user_id='.$manager_user_id.' and r.employee_user_id='.$user_id;exit;
		$query = $this->db->query('select r.id,r.employee_user_id,r.manager_user_id,r.question_id,r.rating_by_manager,r.comment_by_manager,r.extra_achivement_by_manager,r.shortfall_in_performance_reason,r.last_updated_on,r.status,q.question from rating_form r INNER JOIN questions q where q.id=r.question_id  and r.manager_user_id='.$manager_user_id.' and r.employee_user_id='.$user_id);
		return $query->result();
	}
	public function GetDetails($user_id)
	{
		
		$query = $this->db->query('select employee_user_id,question_id from rating_form where employee_user_id='.$user_id);
		return $query->result();
	}
	// public function add_manager_rating($id)
	// {
	// 	// echo $id;exit;
	// 	 	// $user_id= $this->session->userdata('id');
 //            $get_detail=$this->GetDetails($id);
 //            // echo "<pre>";
 //            // print_r($get_detail);exit;

 //            $employee_user_id=$get_detail[0]->user_id;
 //            $manager_user_id=$get_detail[0]->reporting_manager_id;
 //            // echo $manager_user_id;exit;
	// 		$job_knowledge=$_POST['customRadio4'];
 //            $qow=$_POST['customRadio5'];
 //            $iam=$_POST['customRadio6'];
 //            $achievement=$_POST['achievement'];
 //            $shortfall=$_POST['shortfall'];
 //            $comment_by_manager=$_POST['comment_by_manager'];
 //            $rating_by_employee=($job_knowledge+$qow+$iam)/3;
 //            $total_score_by_manager=$job_knowledge+$qow+$iam;
 //            // echo $total_score_by_employee;exit;

 //            $question1=array("ques_id"=>"1","rating"=>$job_knowledge);
 //            $question2=array("ques_id"=>"2","rating"=>$qow);
 //            $question3=array("ques_id"=>"3","rating"=>$iam);
 //            $question4=array("ques_id"=>"4","achievement"=>$achievement);
 //            // $question5=array("ques_id"=>"5","shortfall"=>$shortfall);
 //            $question6=array("ques_id"=>"6","comment_by_manager"=>$comment_by_manager);
 //            $array_comb=array();
            
 //            array(array_push($array_comb,$question1,$question2,$question3,$question4,$question5,$question6));
 //            	// $qqq="$"."question".$j;
 //            	 // $array_comb= $question1+$question2;
 //            	 print_r($array_comb);echo "<br/>";echo "<br/>";echo "<br/>";
 //        	 // for($i=0;$i<count($get_detail);$i++)
 //        	 // {
 //        	 	// echo $gt['question_id'];echo "<br/>";
 //           	foreach($array_comb as  $val)
 //           	{
 //        	 	// if($get_detail[$i]->question_id==$val[
 //        	 	// 	'ques_id']){
 //           // 		print_r($val);echo "<br/>";

 //           	if($val['rating'] ==''){


 //           	if($val['comment_by_manager']!='')
 //           	{
	// 		$data = array(
	// 		'comment_by_manager'=>$val['comment_by_manager'],
	// 		'total_score_by_manager'=>$total_score_by_manager
			
	// 		);

 //       		$this->db->where('employee_user_id',$id);
 //       		$this->db->where('question_id',$val['ques_id']);
	// 		$this->db->update('rating_form',$data);
 //       		}   		
			

	// 		elseif($val['achievement']!=''){
	// 		$data = array(
	// 		'extra_achivement_by_manager'=>$val['achievement'],
	// 		'total_score_by_manager'=>$total_score_by_manager
			
	// 		);

 //       	           	// print_r($data);echo "<br/>";echo "<br/>";echo "<br/>";
	// 		$this->db->where('employee_user_id',$id);
	// 		$this->db->where('question_id',$val['ques_id']);
	// 		$this->db->update('rating_form',$data);
	// 	}
		
			
	// 	}
	// 	else{
	// 		// foreach($get_detail as $gt)
	// 		// {
	// 		$data = array(
	// 		'rating_by_manager'=>$val['rating'],
	// 		'total_score_by_manager'=>$total_score_by_manager
			
	// 		);

 //       	           	// print_r($data);echo "<br/>";echo "<br/>";echo "<br/>";
	// 	$this->db->where('employee_user_id',$id);
	// 	$this->db->where('question_id',$val['ques_id']);
	// 		$this->db->update('rating_form',$data);
	// 			// }
	// 	}
	// 	}
	// 	// }	
	// // }
	// 	// exit;
	// 	return true;
	// }
         
	// public function delete($id)
	// {
	// 	$query = $this->db->query('select userD.user_id as user_id from user_details userD  join user userD_A where userD.user_id = userD_A.id and userD.id='.$id);
	// 	echo "<pre>";
	// 	$user_id=$query->result()[0]->user_id;
	// 	$this->db->where('id',$id);
	// 	$this->db->delete('user_details');

	// 	$this->db->where('id',$user_id);
	// 	$this->db->delete('user');
	// 	return true;
	
	// }
	
	
	// public function getRecord($id)
	// {
 //           // return $id;exit;
	// 	$q= $this->db->get_where('department',array('id'=>$id));
 //          // echo $q;exit;     
	// 	return $q->row_array();
	// }
       
	// public function activate_blogs($id)
	// {
	// 	$data = array('status'=>'1');
	// 	$this->db->where('id',$id);
	// 	$this->db->update('blogs',$data);	
	// }
        

	// public function deactivate_blogs($id)
	// {
	// 	$data = array('status'=>'0');
	// 	$this->db->where('id',$id);
	// 	$this->db->update('blogs',$data);	
	// }
 //    public function checkRatingIdExist($employee_id){
 //    	$count=0;
 //    	// echo $count;exit;
	// 	$result=$this->db->query("select id,rating_by_manager from rating_form where employee_user_id=$employee_id;");	
	// 	$run=$result->result();
	// 	// print_r($run);exit;	
	// 	 for($i=0;$i<count($run) ;$i++)
	// 	 {
	// 	 	if($run[$i]->rating_by_manager!="")
	// 	 	{
	// 	 		$count++;
	// 	 	}
	// 	 }
	// 	 // echo $count;exit;
	// 	 return $count;

	// }
	// public function checkDepartmentNameExistWithId($department_name,$id){
	// 	$result=$this->db->query("select * from department where ltrim(department_name)='$department_name' and id!='$id';");
	// 	return $result->num_rows();	
	// }
   
}