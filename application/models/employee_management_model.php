<?php 
class Employee_management_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
		
	}	
 	
	
	 public function get_employee_management_details()
	{
		// echo 'select userD.id as emp_id, userD.employee_id, userD.employee_name, userD_A.employee_name as reporting_manager_name, dept.department_name,desG.designation_name from user_details userD_A , user_details  userD join department dept on userD.department_id = dept.id join designation desG on userD.designation_id =desG.id where userD.reporting_manager_id=userD_A.user_id';exit;
		
		$query = $this->db->query('select userD.id as emp_id, userD.employee_id, userD.employee_name, userD_A.employee_name as reporting_manager_name, dept.department_name,desG.designation_name from user_details userD_A , user_details  userD join department dept on userD.department_id = dept.id join designation desG on userD.designation_id =desG.id where userD.reporting_manager_id=userD_A.user_id order by userD.employee_name asc');
		// echo "<pre>";
		// print_r($query->result());exit;
		return $query->result();
		
	}
	 public function get_employee_management_detailsWithId($id)
	{
		
		$query = $this->db->query('select userD.id as emp_id,userD.designation_id,userD.department_id,userD.reporting_manager_id,userD.current_gross_salary,userD.current_net_salary,userD.incentive,userD.total_leave_taken,userD.date_of_joining,userD.last_appraisal_date,userD.appraisal_period_from,userD.appraisal_period_to, userD.employee_id, userD.employee_name,userB.email,userB.mobile_no, userB.is_admin,userB.is_manager,userB.is_team_leader,userB.is_hr_admin,userD.user_id,userD_A.employee_name as reporting_manager_name, dept.department_name,desG.designation_name from user_details userD_A , user_details  userD join department dept on userD.department_id = dept.id join designation desG on userD.designation_id =desG.id join  user userB on userB.id=userD.user_id where userD.reporting_manager_id=userD_A.user_id and userD.id='.$id);
		// echo "<pre>";
		// print_r($query->result());exit;
		return $query->result();
		
	}
	public function getallmanager()
	{
		$query= $this->db->query('select id,username,is_hr_admin,is_manager from user where is_hr_admin=1 or is_manager=1 or is_admin=1');
		return $query->result();
	}
	// function clean($string) {
 //     $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
 //     $string = str_replace('--', '-', $string);
 //     $string= str_replace(",","-",strtolower($string));  
 //     return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
 //  	}
	public function add_employee()
	{
			$department_id=$_POST['department_id'];
            $designation_id=$_POST['designation_id'];
            $name_of_appraisee=$_POST['name_of_appraisee'];
            $employee_id=$_POST['employee_id'];
            $current_gross_salary=$_POST['current_gross_salary'];
            $current_net_salary=$_POST['current_net_salary'];
            $incentives=$_POST['incentives'];
            $total_leaves_taken=$_POST['total_leaves_taken'];
            $reporting_manager=$_POST['reporting_manager'];
             $authority=$_POST['authority'];
            if($authority=='manager')
            {
            	$is_admin=0;
            	$hr_admin=0;
            	$manager=1;
            	$team_lead=0;
            }
            elseif($authority=='HR')
            {
            	$is_admin=0;
            	$hr_admin=1;
            	$manager=0;
            	$team_lead=0;

            }
            elseif($authority=='team_lead')
            {
            	$is_admin=0;
            	$hr_admin=0;
            	$manager=0;
            	$team_lead=1;

            }
            elseif($authority=='Admin'){
            	$hr_admin=0;
            	$is_admin=1;
            	// echo $hr_admin;exit;
            	$manager=0;
            	$team_lead=0;
            }
            elseif($authority =='')
            {
            	$hr_admin=0;
            	$is_admin=0;
            	// echo $hr_admin;exit;
            	$manager=0;
            	$team_lead=0;

            }
            $email=$_POST['email'];
            $contact_no=$_POST['contact_no'];
             $date_of_joining=$_POST['date_of_joining'];
            $last_appraisal_date=$_POST['last_appraisal_date'];
            $appraisal_period_from=$_POST['appraisal_period_from'];
            $appraisal_period_to=$_POST['appraisal_period_to'];
		// echo $department_name;exit;
		// $blog_page_key_url=$this->clean($_POST['blog_page_key_url']);	
		$data = array('username'=>$name_of_appraisee,
			'email'=>$email,
			'mobile_no'=> $contact_no,
			'password'=>'orbis123',
			'is_admin'=>$is_admin,
			'is_hr_admin'=>$hr_admin,
			'is_manager'=>$manager,
			'is_team_leader'=>$team_lead,
				'status'=>'1');
       	           	
		$this->db->insert('user',$data);
		$user_id= $this->db->insert_id();
		
		$user_detail_data=array('user_id'=>$user_id,
			'employee_id'=>$employee_id,
			'employee_name'=>$name_of_appraisee,
			'department_id'=>$department_id,
			'designation_id'=>$designation_id,
			'reporting_manager_id'=>$reporting_manager,
			'current_gross_salary'=>$current_gross_salary,
			'current_net_salary'=>$current_net_salary,
			'incentive'=>$incentives,
			'total_leave_taken'=>$total_leaves_taken,
			'date_of_joining'=>$date_of_joining,
			'last_appraisal_date'=>$last_appraisal_date,
			'appraisal_period_from'=>$appraisal_period_from,
			'appraisal_period_to'=>$appraisal_period_to,
			'status'=>'1'


	);
		$this->db->insert('user_details',$user_detail_data);
		$user_details_id= $this->db->insert_id();
		
		return $user_details_id;
	}
        
	
	
     public function edit_employee()
	{
			$emp_id=$_POST['emp_id'];
			$user_id=$_POST['user_id'];
			$department_id=$_POST['department_id'];
            $designation_id=$_POST['designation_id'];
            $name_of_appraisee=$_POST['name_of_appraisee'];
            $employee_id=$_POST['employee_id'];
            $current_gross_salary=$_POST['current_gross_salary'];
            $current_net_salary=$_POST['current_net_salary'];
            $incentives=$_POST['incentives'];
            $total_leaves_taken=$_POST['total_leaves_taken'];
            $reporting_manager=$_POST['reporting_manager'];
            $authority=$_POST['authority'];
            // echo $reporting_manager;exit;
            if($authority=='Manager')
            {
            	$is_admin=0;
            	$hr_admin=0;
            	$manager=1;
            	$team_lead=0;
            }
            elseif($authority=='HR')
            {
            	$is_admin=0;
            	$hr_admin=1;
            	$manager=0;
            	$team_lead=0;

            }
            elseif($authority=='team_lead')
            {
            	$is_admin=0;
            	$hr_admin=0;
            	$manager=0;
            	$team_lead=1;

            }
             elseif($authority=='Admin'){
            	$hr_admin=0;
            	$is_admin=1;
            	// echo $hr_admin;exit;
            	$manager=0;
            	$team_lead=0;
            }
            elseif($authority =='')
            {
            	$hr_admin=0;
            	$is_admin=0;
            	// echo $hr_admin;exit;
            	$manager=0;
            	$team_lead=0;

            }

             $email=$_POST['email'];
            $contact_no=$_POST['contact_no'];
             $date_of_joining=$_POST['date_of_joining'];
            $last_appraisal_date=$_POST['last_appraisal_date'];
            $appraisal_period_from=$_POST['appraisal_period_from'];
            $appraisal_period_to=$_POST['appraisal_period_to'];

		// echo $this->db->last_query();exit;
		$data = array('username'=>$name_of_appraisee,
			'email'=>$email,
			'mobile_no'=> $contact_no,
			'password'=>'orbis123',
			'is_admin'=>$is_admin,
			'is_hr_admin'=>$hr_admin,
			'is_manager'=>$manager,
			'is_team_leader'=>$team_lead,
				'status'=>'1');

		
		//echo $requestforquote_id;exit;
		// print_r($data);exit;
		$this->db->where('id',$user_id);
		$this->db->update('user',$data);
		

		$user_detail_data=array('user_id'=>$user_id,
			'employee_id'=>$employee_id,
			'employee_name'=>$name_of_appraisee,
			'department_id'=>$department_id,
			'designation_id'=>$designation_id,
			'reporting_manager_id'=>$reporting_manager,
			'current_gross_salary'=>$current_gross_salary,
			'current_net_salary'=>$current_net_salary,
			'incentive'=>$incentives,
			'total_leave_taken'=>$total_leaves_taken,
			'date_of_joining'=>$date_of_joining,
			'last_appraisal_date'=>$last_appraisal_date,
			'appraisal_period_from'=>$appraisal_period_from,
			'appraisal_period_to'=>$appraisal_period_to,
			'status'=>'1'


	);
		$this->db->where('id',$emp_id);
		$this->db->update('user_details',$user_detail_data);
		return TRUE;

		
	}
         
	public function delete($id)
	{
		$query = $this->db->query('select userD.user_id as user_id from user_details userD  join user userD_A where userD.user_id = userD_A.id and userD.id='.$id);
		echo "<pre>";
		$user_id=$query->result()[0]->user_id;
		$this->db->where('id',$id);
		$this->db->delete('user_details');

		$this->db->where('id',$user_id);
		$this->db->delete('user');
		return true;
	
	}
	
	
	// public function getRecord($id)
	// {
 //           // return $id;exit;
	// 	$q= $this->db->get_where('department',array('id'=>$id));
 //          // echo $q;exit;     
	// 	return $q->row_array();
	// }
       
	// public function activate_blogs($id)
	// {
	// 	$data = array('status'=>'1');
	// 	$this->db->where('id',$id);
	// 	$this->db->update('blogs',$data);	
	// }
        

	// public function deactivate_blogs($id)
	// {
	// 	$data = array('status'=>'0');
	// 	$this->db->where('id',$id);
	// 	$this->db->update('blogs',$data);	
	// }
    public function checkEmployeeIdExist($employee_id){
		$result=$this->db->query("select * from user_details where employee_id=$employee_id;");		
		return $result->num_rows();
	}
	public function checkDepartmentNameExistWithId($department_name,$id){
		$result=$this->db->query("select * from department where ltrim(department_name)='$department_name' and id!='$id';");
		return $result->num_rows();	
	}
   
}