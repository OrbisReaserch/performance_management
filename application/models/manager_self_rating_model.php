<?php 
error_reporting(0);
class Manager_self_rating_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
		
	}	
 	
	
	 public function get_employee_appraisee_details()
	{
		
		
		$query = $this->db->query('select userD.id as emp_id, userD.employee_id, userD.employee_name, userD_A.employee_name as reporting_manager_name, dept.department_name,desG.designation_name from user_details userD_A , user_details  userD join department dept on userD.department_id = dept.id join designation desG on userD.designation_id =desG.id where userD.reporting_manager_id=userD_A.user_id');
		// echo "<pre>";
		// print_r($query->result());exit;
		return $query->result();
		
	}
	 public function get_employee_management_detailsWithId($id)
	{
		
		$query = $this->db->query('select userD.id as emp_id,userD.designation_id,userD.department_id,userD.current_gross_salary,userD.current_net_salary,userD.incentive,userD.total_leave_taken,userD.date_of_joining,userD.last_appraisal_date,userD.appraisal_period_from,userD.appraisal_period_to, userD.employee_id, userD.employee_name,userB.email,userB.mobile_no, userB.is_admin,userB.is_manager,userB.is_team_leader,userB.is_hr_admin,userD.user_id,userD_A.employee_name as reporting_manager_name, dept.department_name,desG.designation_name from user_details userD_A , user_details  userD join department dept on userD.department_id = dept.id join designation desG on userD.designation_id =desG.id join  user userB on userB.id=userD.user_id where userD.reporting_manager_id=userD_A.user_id and userD.id='.$id);
		// echo "<pre>";
		// print_r($query->result());exit;
		return $query->result();
		
	}
	
	// function clean($string) {
 //     $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
 //     $string = str_replace('--', '-', $string);
 //     $string= str_replace(",","-",strtolower($string));  
 //     return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
 //  	}
	public function GetDetails($user_id)
	{
		
		$query = $this->db->query('select user_id,reporting_manager_id from user_details where user_id='.$user_id);
		return $query->result();
	}
	public function filled_form_details($user_id)
	{
		// echo $user_id;exit;
		$query = $this->db->query('select r.id,r.employee_user_id,r.manager_user_id,r.question_id,r.rating_by_employee,r.rating_by_manager,r.total_score_by_employee,r.comment_by_employee,r.comment_by_manager,r.extra_achivement_by_employee,r.extra_achivement_by_manager,r.shortfall_in_performance_reason,r.last_updated_on,r.status,q.question from rating_form r INNER JOIN questions q where q.id=r.question_id and employee_user_id='.$user_id);
		return $query->result();
	}
	public function filled_form_details_by_manager($user_id)
	{
		$qry=$this->db->query('select reporting_manager_id from user_details where user_id='.$user_id);
		$rp_detail=$qry->result();
		// print_r($qry->result());exit;
		$user_id= $this->session->userdata('id');
		 $manager_user_id= $rp_detail[0]->reporting_manager_id;
		$query = $this->db->query('select r.id,r.employee_user_id,r.manager_user_id,r.question_id,r.rating_by_manager,r.comment_by_manager,r.extra_achivement_by_manager,r.shortfall_in_performance_reason,r.last_updated_on,r.status,q.question from rating_form r INNER JOIN questions q where q.id=r.question_id  and r.manager_user_id='.$manager_user_id.' and r.employee_user_id='.$user_id);
		return $query->result();
	}
	public function checkRatingIdExist($user_id)
	{
		$query = $this->db->query('select employee_user_id from rating_form where employee_user_id='.$user_id);
		return(count($query->result()));

	}
	public function GetQuestionDetails()
	{
		$query = $this->db->query('select id,question from questions ');
		return $query->result();
	}
	public function add_manager_self_rating()
	{
		 	$user_id= $this->session->userdata('id');
            $get_detail=$this->GetDetails($user_id);
            // print_r($get_detail);exit;

            $employee_user_id=$get_detail[0]->user_id;
            // $manager_user_id=$get_detail[0]->reporting_manager_id;
            // echo $manager_user_id;exit;
			$job_knowledge=$_POST['customRadio4'];
            $qow=$_POST['customRadio5'];
            $iam=$_POST['customRadio6'];
            $tm=$_POST['customRadio7'];
            // echo $tm;exit;
            $timemg=$_POST['customRadio8'];
            $comm=$_POST['customRadio9'];
            $proactive=$_POST['customRadio10'];
            $attendance=$_POST['customRadio11'];
            $discipline=$_POST['customRadio12'];
            $gc=$_POST['customRadio13'];
            $achievement=$_POST['achievement'];
            $shortfall=$_POST['shortfall'];
            $comment_by_appraisee=$_POST['comment_by_appraisee'];
            // $rating_by_employee=($job_knowledge+$qow+$iam)/3;
            $total_score_by_employee=$job_knowledge+$qow+$iam;
            // echo $total_score_by_employee;exit;
            $get_ques_detail=$this->GetQuestionDetails();
            // print_r($get_ques_detail);exit;
            for($i = 0; $i <= count($get_ques_detail); $i++) {
				     ${"questionno$i"} = $get_ques_detail[$i]->id;
				}
				// echo $question1;echo "<br/>";
				// echo $question2;echo "<br/>";
				// echo $question3;echo "<br/>";
				echo $question3;echo "<br/>";
				// echo $question5;echo "<br/>";
				// // echo $question6;echo "<br/>";
				// echo $question0;echo "<br/>";
				// exit;
            $question1=array("ques_id"=>$questionno0,"rating"=>$job_knowledge);
            $question2=array("ques_id"=>$questionno1,"rating"=>$qow);
            $question3=array("ques_id"=>$questionno2,"rating"=>$iam);
            $question4=array("ques_id"=>$questionno3,"rating"=>$tm);
            // print_r($question4);exit;
            $question5=array("ques_id"=>$questionno4,"rating"=>$timemg);
            $question6=array("ques_id"=>$questionno5,"rating"=>$comm);
            $question7=array("ques_id"=>$questionno6,"rating"=>$proactive);
            $question8=array("ques_id"=>$questionno7,"rating"=>$attendance);
            $question9=array("ques_id"=>$questionno8,"rating"=>$discipline);
            $question10=array("ques_id"=>$questionno9,"rating"=>$gc);
            $question11=array("ques_id"=>$questionno10,"achievement"=>$achievement);
            $question12=array("ques_id"=>$questionno11,"shortfall"=>$shortfall);
            $question13=array("ques_id"=>$questionno12,"comment_by_appraisee"=>$comment_by_appraisee);
            $array_comb=array();
            
            array(array_push($array_comb,$question1,$question2,$question3,$question4,$question5,$question6,$question7,$question8,$question9,$question10,$question11,$question12,$question13));
            	// $qqq="$"."question".$j;
            	 // $array_comb= $question1+$question2;
            	 // print_r($array_comb);echo "<br/>";echo "<br/>";echo "<br/>";exit;
           	foreach($array_comb as  $val)
           	{
           		// print_r($val);exit;
           	if($val['rating'] ==''){
           	if($val['comment_by_appraisee']!='')
           	{
			$data = array('employee_user_id'=>$employee_user_id,
			// 'manager_user_id'=>$manager_user_id,
			'question_id'=> $val['ques_id'],
			'total_score_by_employee'=>$total_score_by_employee,
			'comment_by_employee'=>$val['comment_by_appraisee'],
			'status'=>'1');

       		print_r($data);echo "<br/>";echo "<br/>";echo "<br/>";
			$this->db->insert('rating_form',$data);
			$user_id= $this->db->insert_id();
       		}   		
			

			elseif($val['achievement']!=''){
			$data = array('employee_user_id'=>$employee_user_id,
			// 'manager_user_id'=>$manager_user_id,
			'question_id'=> $val['ques_id'],
			'total_score_by_employee'=>$total_score_by_employee,
			'extra_achivement_by_employee'=>$val['achievement'],
			'status'=>'1');

       	           	// print_r($data);echo "<br/>";echo "<br/>";echo "<br/>";
				$this->db->insert('rating_form',$data);
				$user_id= $this->db->insert_id();
		}
		elseif($val['shortfall']!=''){
		$data = array('employee_user_id'=>$employee_user_id,
			// 'manager_user_id'=>$manager_user_id,
			'question_id'=> $val['ques_id'],
			'total_score_by_employee'=>$total_score_by_employee,
			'shortfall_in_performance_reason'=>$val['shortfall'],
				'status'=>'1');

       	           	// print_r($data);echo "<br/>";echo "<br/>";echo "<br/>";
		$this->db->insert('rating_form',$data);
		$user_id= $this->db->insert_id();
		}
			
		}
		else{
			$data = array('employee_user_id'=>$employee_user_id,
			// 'manager_user_id'=>$manager_user_id,
			'question_id'=> $val['ques_id'],
			'rating_by_employee'=>$val['rating'],
			'total_score_by_employee'=>$total_score_by_employee,
			'status'=>'1');

       	           	// print_r($data);echo "<br/>";echo "<br/>";echo "<br/>";
		$this->db->insert('rating_form',$data);
		$user_id= $this->db->insert_id();

		}
		}	
		return true;
	}
        
	
	
 //     public function edit_employee()
	// {
	// 		$emp_id=$_POST['emp_id'];
	// 		$user_id=$_POST['user_id'];
	// 		$department_id=$_POST['department_id'];
 //            $designation_id=$_POST['designation_id'];
 //            $name_of_appraisee=$_POST['name_of_appraisee'];
 //            $employee_id=$_POST['employee_id'];
 //            $current_gross_salary=$_POST['current_gross_salary'];
 //            $current_net_salary=$_POST['current_net_salary'];
 //            $incentives=$_POST['incentives'];
 //            $total_leaves_taken=$_POST['total_leaves_taken'];
 //            $reporting_manager=$_POST['reporting_manager'];
 //            $authority=$_POST['authority'];
 //            if($authority=='Manager')
 //            {

 //            	$hr_admin=0;
 //            	$manager=1;
 //            	$team_lead=0;
 //            }
 //            elseif($authority=='HR')
 //            {

 //            	$hr_admin=1;
 //            	$manager=0;
 //            	$team_lead=0;

 //            }
 //            elseif($authority=='team_lead')
 //            {

 //            	$hr_admin=0;
 //            	$manager=0;
 //            	$team_lead=1;

 //            }
 //             $email=$_POST['email'];
 //            $contact_no=$_POST['contact_no'];
 //             $date_of_joining=$_POST['date_of_joining'];
 //            $last_appraisal_date=$_POST['last_appraisal_date'];
 //            $appraisal_period_from=$_POST['appraisal_period_from'];
 //            $appraisal_period_to=$_POST['appraisal_period_to'];

	// 	// echo $this->db->last_query();exit;
	// 	$data = array('username'=>$name_of_appraisee,
	// 		'email'=>$email,
	// 		'mobile_no'=> $contact_no,
	// 		'password'=>'orbis123',
	// 		'is_hr_admin'=>$hr_admin,
	// 		'is_manager'=>$manager,
	// 		'is_team_leader'=>$team_lead,
	// 			'status'=>'1');

		
	// 	//echo $requestforquote_id;exit;
	// 	// print_r($data);exit;
	// 	$this->db->where('id',$user_id);
	// 	$this->db->update('user',$data);
		

	// 	$user_detail_data=array('user_id'=>$user_id,
	// 		'employee_id'=>$employee_id,
	// 		'employee_name'=>$name_of_appraisee,
	// 		'department_id'=>$department_id,
	// 		'designation_id'=>$designation_id,
	// 		'reporting_manager_id'=>'3',
	// 		'current_gross_salary'=>$current_gross_salary,
	// 		'current_net_salary'=>$current_net_salary,
	// 		'incentive'=>$incentives,
	// 		'total_leave_taken'=>$total_leaves_taken,
	// 		'date_of_joining'=>$date_of_joining,
	// 		'last_appraisal_date'=>$last_appraisal_date,
	// 		'appraisal_period_from'=>$appraisal_period_from,
	// 		'appraisal_period_to'=>$appraisal_period_to,
	// 		'status'=>'1'


	// );
	// 	$this->db->where('id',$emp_id);
	// 	$this->db->update('user_details',$user_detail_data);
	// 	return TRUE;

		
	// }
         
	// public function delete($id)
	// {
	// 	$query = $this->db->query('select userD.user_id as user_id from user_details userD  join user userD_A where userD.user_id = userD_A.id and userD.id='.$id);
	// 	echo "<pre>";
	// 	$user_id=$query->result()[0]->user_id;
	// 	$this->db->where('id',$id);
	// 	$this->db->delete('user_details');

	// 	$this->db->where('id',$user_id);
	// 	$this->db->delete('user');
	// 	return true;
	
	// }
	
	
	// public function getRecord($id)
	// {
 //           // return $id;exit;
	// 	$q= $this->db->get_where('department',array('id'=>$id));
 //          // echo $q;exit;     
	// 	return $q->row_array();
	// }
       
	// public function activate_blogs($id)
	// {
	// 	$data = array('status'=>'1');
	// 	$this->db->where('id',$id);
	// 	$this->db->update('blogs',$data);	
	// }
        

	// public function deactivate_blogs($id)
	// {
	// 	$data = array('status'=>'0');
	// 	$this->db->where('id',$id);
	// 	$this->db->update('blogs',$data);	
	// }
 //    public function checkEmployeeIdExist($employee_id){
	// 	$result=$this->db->query("select * from user_details where employee_id=$employee_id;");		
	// 	return $result->num_rows();
	// }
	// public function checkDepartmentNameExistWithId($department_name,$id){
	// 	$result=$this->db->query("select * from department where ltrim(department_name)='$department_name' and id!='$id';");
	// 	return $result->num_rows();	
	// }
   
}