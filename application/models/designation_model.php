<?php 
class Designation_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
		 $this->load->database();
		
	}	
 	
	
	 public function getall()
	{
		$query = $this->db->query('select d.id,d.designation_name,d.department_id,d.last_updated_on,d.status,dpt.department_name from designation d INNER JOIN department dpt where d.department_id=dpt.id and d.status=1 order by d.designation_name asc');
		
		return $query->result();
		
	}
	// function clean($string) {
 //     $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
 //     $string = str_replace('--', '-', $string);
 //     $string= str_replace(",","-",strtolower($string));  
 //     return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
 //  	}
	public function add_designation()
	{
		$department_id=$_POST['department_id'];
		$designation_name = $_POST['designation_name'];
		// echo $department_name;exit;
		// $blog_page_key_url=$this->clean($_POST['blog_page_key_url']);	
		$data = array('designation_name'=>$designation_name,
			'department_id'=>$department_id,
				'status'=>'1');
       	           	
		$this->db->insert('designation',$data);
		$did= $this->db->insert_id();
		
		
		return $did;
	}
        
	
	
     public function edit_designation()
	{
		$designation_name = $_POST['designation_name'];
		$designation_id=$_POST['designation_id'];
        $department_id=$_POST['department_id'];

		//$blog_page_key_url=$this->clean($_POST['blog_page_key_url']);

		$data = array(
		'designation_name'=>$designation_name,
		'department_id'=>$department_id
		);
		//echo $requestforquote_id;exit;
		// print_r($data);exit;
		$this->db->where('id',$designation_id);
		$this->db->update('designation',$data);
		// echo $this->db->last_query();exit;
		return TRUE;
		
	}
         
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('designation');
		return true;
	
	}
	
	
	public function getRecord($id)
	{
		//echo "select id,designation_name,department_id,status from designation  where id=".$id." and status=1 order by id desc";exit;
           // echo $id;exit;
		// $q= $this->db->get_where('designation',array('id'=>$id));
		 $query=$this->db->query("select id,designation_name,department_id,status from designation  where id=".$id." and status=1 order by id desc");
          // echo $query;exit;     
		// return $q->row_array();
		return $query->result();
	}
	public function getRecordForDesignationName($id)
	{
           // return $id;exit;
		$q= $this->db->get_where('designation',array('department_id'=>$id));
          // echo $q;exit;     
		// return $q->row_array();
		return $q->result();
	}
	
	public function getRecordForDepartment()
	{
           // return $id;exit;
		// $q= $this->db->get_where('designation',array('id'=>$id));
          // echo $q;exit; 
		$query = $this->db->query('select id,department_name,status from department where status=1 order by department_name asc');
		
		return $query->result();
      
          // $query=$this->db->get_where('department',array('id'=>$id))  ;
		// return $q->row_array();
		// return $query->result();
	}
       
	// public function activate_blogs($id)
	// {
	// 	$data = array('status'=>'1');
	// 	$this->db->where('id',$id);
	// 	$this->db->update('blogs',$data);	
	// }
        

	// public function deactivate_blogs($id)
	// {
	// 	$data = array('status'=>'0');
	// 	$this->db->where('id',$id);
	// 	$this->db->update('blogs',$data);	
	// }
    public function checkDesignationNameExist($designation_name){
		$result=$this->db->query("select * from designation where ltrim(designation_name)='$designation_name';");		
		return $result->num_rows();
	}
	public function checkDepartmentNameExistWithId($department_name,$id){
		$result=$this->db->query("select * from designation where ltrim(designation_name)='$designation_name' and id!='$id';");
		return $result->num_rows();	
	}
   
}