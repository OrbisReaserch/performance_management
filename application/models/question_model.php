<?php 
class Question_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
		 $this->load->database();
		
	}	
 	
	
	 public function getall()
	{
		$query = $this->db->query('select q.id,q.question,q.department_id,q.last_updated_on,q.status,d.department_name from questions q INNER JOIN department d where d.id=q.department_id and q.status=1 order by last_updated_on desc');
		
		return $query->result();
		
	}
	public function get_question_details_appraisee(){
		$query = $this->db->query('select q.id,q.question,q.department_id,q.flag,q.last_updated_on,q.status,d.department_name from questions q INNER JOIN department d where d.id=q.department_id and q.status=1');
		
		return $query->result();
	}
	public function get_department()
	{
		$query = $this->db->query('select id,department_name,last_updated_on,status from department where status=1 order by last_updated_on desc');
		
		return $query->result();
	}
	public function getRecord($id)
	{
		$query = $this->db->query('select id,question,flag,department_id,last_updated_on,status from questions where id='.$id);
		
		return $query->result();
	}
	// function clean($string) {
 //     $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
 //     $string = str_replace('--', '-', $string);
 //     $string= str_replace(",","-",strtolower($string));  
 //     return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
 //  	}
	public function add_question()
	{
		$department_id=$_POST['department_id'];
		$question_name = $_POST['question'];
		$flag=$_POST['flag'];
		// echo $department_name;exit;
		// $blog_page_key_url=$this->clean($_POST['blog_page_key_url']);	
		$data = array('question'=>$question_name,
			'department_id'=>$department_id,
			'flag' =>$flag,
				'status'=>'1');
       	   // print_r($data);exit;        	
		$this->db->insert('questions',$data);
		$qid= $this->db->insert_id();
		
		
		return $qid;
	}
        
	
	
     public function edit_question($id)
	{
		$question_name = $_POST['question'];
		// $designation_id=$_POST['designation_id'];
		$flag=$_POST['flag'];
        $department_id=$_POST['department_id'];

		//$blog_page_key_url=$this->clean($_POST['blog_page_key_url']);

		$data = array(
		'question'=>$question_name,
		'department_id'=>$department_id,
		'flag' =>$flag,
		);
		//echo $requestforquote_id;exit;
		// echo $id;echo "<br/>";
		// print_r($data);exit;
		$this->db->where('id',$id);
		$this->db->update('questions',$data);
		// echo $this->db->last_query();exit;
		return TRUE;
		
	}
         
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('questions');
		return true;
	
	}
	
	
	// public function getRecord($id)
	// {
	// 	//echo "select id,designation_name,department_id,status from designation  where id=".$id." and status=1 order by id desc";exit;
 //           // echo $id;exit;
	// 	// $q= $this->db->get_where('designation',array('id'=>$id));
	// 	 $query=$this->db->query("select id,designation_name,department_id,status from designation  where id=".$id." and status=1 order by id desc");
 //          // echo $query;exit;     
	// 	// return $q->row_array();
	// 	return $query->result();
	// }
	// public function getRecordForDesignationName($id)
	// {
 //           // return $id;exit;
	// 	$q= $this->db->get_where('designation',array('department_id'=>$id));
 //          // echo $q;exit;     
	// 	// return $q->row_array();
	// 	return $q->result();
	// }
	
	// public function getRecordForDepartment()
	// {
 //           // return $id;exit;
	// 	// $q= $this->db->get_where('designation',array('id'=>$id));
 //          // echo $q;exit; 
	// 	$query = $this->db->query('select id,department_name,status from department where status=1 order by department_name asc');
		
	// 	return $query->result();
      
 //          // $query=$this->db->get_where('department',array('id'=>$id))  ;
	// 	// return $q->row_array();
	// 	// return $query->result();
	// }
       
	// // public function activate_blogs($id)
	// // {
	// // 	$data = array('status'=>'1');
	// // 	$this->db->where('id',$id);
	// // 	$this->db->update('blogs',$data);	
	// // }
        

	// // public function deactivate_blogs($id)
	// // {
	// // 	$data = array('status'=>'0');
	// // 	$this->db->where('id',$id);
	// // 	$this->db->update('blogs',$data);	
	// // }
    public function checkQuestionNameExist($question_name){
		$result=$this->db->query("select * from questions where ltrim(question)='$question_name';");		
		return $result->num_rows();
	}
	// public function checkQuestionNameExist($department_name,$id){
	// 	$result=$this->db->query("select * from designation where ltrim(designation_name)='$designation_name' and id!='$id';");
	// 	return $result->num_rows();	
	// }
   
}