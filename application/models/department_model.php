<?php 
class Department_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
		
	}	
 	
	
	 public function getall()
	{
		$query = $this->db->query('select department_name, id, last_updated_on from department order by department_name asc');
		
		return $query->result();
		
	}
	// function clean($string) {
 //     $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
 //     $string = str_replace('--', '-', $string);
 //     $string= str_replace(",","-",strtolower($string));  
 //     return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
 //  	}
	public function add_department()
	{
		$department_name = $_POST['department_name'];
		// echo $department_name;exit;
		// $blog_page_key_url=$this->clean($_POST['blog_page_key_url']);	
		$data = array('department_name'=>$department_name,
				'status'=>'1');
       	           	
		$this->db->insert('department',$data);
		$did= $this->db->insert_id();
		
		
		return $did;
	}
        
	
	
     public function edit_department()
	{
		$department_name = $_POST['department_name'];
		$department_id=$_POST['department_id'];
        

		//$blog_page_key_url=$this->clean($_POST['blog_page_key_url']);

		$data = array(
		'department_name'=>$department_name,
		);
		//echo $requestforquote_id;exit;
		// print_r($data);exit;
		$this->db->where('id',$department_id);
		$this->db->update('department',$data);
		// echo $this->db->last_query();exit;
		return TRUE;
		
	}
         
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('department');
		return true;
	
	}
	
	
	public function getRecord($id)
	{
           // return $id;exit;
		$q= $this->db->get_where('department',array('id'=>$id));
          // echo $q;exit;     
		return $q->row_array();
	}
       
	// public function activate_blogs($id)
	// {
	// 	$data = array('status'=>'1');
	// 	$this->db->where('id',$id);
	// 	$this->db->update('blogs',$data);	
	// }
        

	// public function deactivate_blogs($id)
	// {
	// 	$data = array('status'=>'0');
	// 	$this->db->where('id',$id);
	// 	$this->db->update('blogs',$data);	
	// }
    public function checkDepartmentNameExist($department_name){
		$result=$this->db->query("select * from department where ltrim(department_name)='$department_name';");		
		return $result->num_rows();
	}
	public function checkDepartmentNameExistWithId($department_name,$id){
		$result=$this->db->query("select * from department where ltrim(department_name)='$department_name' and id!='$id';");
		return $result->num_rows();	
	}
   
}