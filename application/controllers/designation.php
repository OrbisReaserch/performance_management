<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Designation extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct() 
    {
        parent::__construct();

       
        $this->load->model('department_model');
        $this->load->model('designation_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }
	public function index()
	{
		// $this->load->model('department_model'); 
		$data['department_result'] = $this->department_model->getall(); 
		// $data['result'] = $this->department_model->getall();
		$data['designation_result']=$this->designation_model->getall();
        

		$this->load->view('designation/designation',$data);
	}
	 public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('../login');
        }
    }
     public function add_designation() 
    {
    	
        if(isset($_POST['submit']))
        {
        	
            $designation_name=$_POST['designation_name'];
            $department_id=$_POST['department_id'];
            // echo $department_id;exit;
            $checkexist=$this->designation_model->checkDesignationNameExist($designation_name);
            // echo $checkexist;exit;
            if($checkexist==0)
            {   

            $id = $this->designation_model->add_designation();

            if($id)
            {
            	echo $id;
                                     

                    $this->session->set_flashdata('success', 'Designation has been added successfully.');
                    redirect(base_url().'designation');

            }

            else
            {
                $this->session->set_flashdata('error', 'Unable to save department.');
                   
                redirect(base_url().'designation');
            }
         } 
          else
            {
            	
                $this->session->set_flashdata('error', 'Designation name  already exist');
                
                 redirect(base_url().'designation');
                
            }   
        }
        else
        {
           
            redirect(base_url().'designation');
        }
        
    }
    public function edit_designation($id) 
    {
        // echo $id;exit;
        $data['info'] = $this->designation_model->getRecord($id);
        $data['department_info'] = $this->designation_model->getRecordForDepartment();
        
        // print_r($data['info']);exit;
        if(isset($_POST['submit']))
        {
            // echo "hello";exit;
            $id=$_POST['department_id'];
           // echo $data['info'][0]->id;exit;
            // $this->Checklogin();
             $designation_name=$_POST['designation_name'];
             // echo $id."==".$data['info'][0]->department_id;exit;
            $checkexist=$this->designation_model->checkDesignationNameExist($designation_name);
            if($checkexist==0 ||($id!=$data['info'][0]->department_id))
            {
            // echo "hii";exit;
                 $edit = $this->designation_model->edit_designation();
                 // echo $edit;exit;
                 if($edit)
                 {
                $this->session->set_flashdata('success', 'Designation have been updated successfully.');
                    redirect(base_url().'designation');
                }
                 else
                  {
                $this->session->set_flashdata('error', 'Unable to update Request For Quote.');
               
                 redirect(base_url().'designation');
                  }
           
           
         }else{
                    $this->session->set_flashdata('error', 'Designation name already exist');
                    redirect(base_url().'designation/');
            }   
        }
        else
        {
            
            $this->load->view('designation/designation_edit', $data);
        }
    }
     public function get_designation_name($id) 
    {
         if(isset($_POST['dept_id']))
        {
            // echo $_POST['dept_id'];exit;
             $dept_id=$_POST['dept_id'];
             $data['designation_from_departmentID'] = $this->designation_model->getRecordForDesignationName($dept_id);
             // header("Content-type: application/json; charset=utf-8");
             echo json_encode($data['designation_from_departmentID']);
             // exit;
        }
    }
     public function delete_designation($id) 
    {
        if ($this->designation_model->delete($id)) 
        {
            $this->session->set_flashdata('success', 'Record has been deleted successfully.');
            redirect(base_url().'designation');
        }
    }
	
}
