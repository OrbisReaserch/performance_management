<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manager_dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct() 
    {
        parent::__construct();

       	$this->load->model('manager_dashboard_model');
       	// $this->load->model('employee_appraisee_model');
        $this->load->model('question_model');
        $this->load->model('department_model');
        $this->load->model('designation_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }
	public function index()
	{
		$user_id= $this->session->userdata('id');
		// echo $user_id;exit;
		$data['employee_rating_details']=$this->manager_dashboard_model->get_employee_rating_details($user_id);
		$this->load->view('manager_dashboard/manager_dashboard',$data);
	}
	 public function edit_manager_dashboard($id) 
    {
    	// echo $id;exit;
       
        if(isset($_POST['submit']))
        {
            $emp_id=$_POST['emp_id'];
            $user_id=$_POST['user_id'];
            $department_id=$_POST['department_id'];
            $designation_id=$_POST['designation_id'];
            $name_of_appraisee=$_POST['name_of_appraisee'];
            $employee_id=$_POST['employee_id'];
            $current_gross_salary=$_POST['current_gross_salary'];
            $current_net_salary=$_POST['current_net_salary'];
            $incentives=$_POST['incentives'];
            $total_leaves_taken=$_POST['total_leaves_taken'];
            $reporting_manager=$_POST['reporting_manager'];
            $authority=$_POST['authority'];
            $email=$_POST['email'];
            $contact_no=$_POST['contact_no'];
            $date_of_joining=$_POST['date_of_joining'];
            $last_appraisal_date=$_POST['last_appraisal_date'];
            $appraisal_period_from=$_POST['appraisal_period_from'];
            $appraisal_period_to=$_POST['appraisal_period_to'];

          
                 $edit = $this->employee_management_model->edit_employee();
                 // echo $edit;exit;
                 if($edit)
                 {
                $this->session->set_flashdata('success', 'Rating has been updated successfully.');
                    redirect(base_url().'manager_dashboard');
                }
                 else
                  {
                $this->session->set_flashdata('error', 'Unable to update Request For Quote.');
               
                 redirect(base_url().'manager_dashboard');
                  }
           
           
         // }else{
         //            $this->session->set_flashdata('error', 'Designation name already exist');
         //            redirect(base_url().'designation/');
         //    }   
        }
        else
        {
        	$data['filled_form_details']=$this->manager_dashboard_model->filled_form_details($id);
        	 // print_r($data['filled_form_details']);exit;
        	$data['filled_form_details_by_manager']=$this->manager_dashboard_model->filled_form_details_by_manager($id);
             $data['employee_details']=$this->manager_dashboard_model->employee_details($id);
            $data['question_details']=$this->question_model->get_question_details_appraisee();
            $this->load->view('manager_dashboard/manager_dashboard_edit',$data);
        }
    }
    public function manager_rating_to_employee($id)
    {
    	// echo "hii";exit;
    	$user_id= $this->session->userdata('id');
    	$data['department_result'] = $this->department_model->getall(); 
		// print_r($data['department_result']);exit;
		// $data['result'] = $this->department_model->getall();
		$data['designation_result']=$this->designation_model->getall();

		if(isset($_POST['submit']))
        {
    	// echo $id;exit;
        	// echo "hello";exit;
           

            $job_knowledge=$_POST['customRadio4'];
            // echo $job_knowledge;echo "</br>";
            $qow=$_POST['customRadio5'];
            $iam=$_POST['customRadio6'];
            $achievement=$_POST['achievement'];
            $shortfall=$_POST['shortfall'];
            $comment_by_appraisee=$_POST['comment_by_appraisee'];
            
            // echo $comment_by_appraisee;exit;
            // $user_id= $this->session->userdata('id');
            // echo  $user_id;exit;
            $checkexist=$this->manager_rating_model->checkRatingIdExist($id);
            // echo $checkexist;exit;
             if($checkexist!=0)
            {  
            	   $data['employee_rating_details']=$this->manager_rating_model->get_employee_rating_details($user_id);
                 $this->session->set_flashdata('error', 'You have already filled the form.');
                $this->load->view('manager_rating/manager_rating',$data);
            }
            else{
                 // echo  $checkexist;exit;
            	// echo"hello";exit;
            	$get_id = $this->manager_rating_model->add_manager_rating($id);
                // echo $id;exit;
            if($get_id)
            {
            	// echo $get_id;exit;
                                     

                    $this->session->set_flashdata('success', 'Rating has been added successfully.');

                    redirect(base_url().'manager_rating');

            }

            else
            {
                $this->session->set_flashdata('error', 'Unable to save department.');
                // $data['include'] = 'department/add_department';
                // $this->load->view('department/department',$data);    
                redirect(base_url().'manager_rating');
            }
            // } 

        }
    }
        else{
		$this->load->view('manager_rating/manager_rating',$data);
		}
	}
	
}
