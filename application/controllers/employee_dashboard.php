<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */
	 public function __construct() 
    {
        parent::__construct();

       	$this->load->model('employee_dashboard_model');
        $this->load->model('question_model');
        $this->load->model('department_model');
        $this->load->model('designation_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }
	public function index()
	{
		$user_id= $this->session->userdata('id');
		// echo $user_id;exit;
		$data['filled_form_details']=$this->employee_dashboard_model->filled_form_details($user_id);
		$data['filled_form_details_by_manager']=$this->employee_dashboard_model->filled_form_details_by_manager($user_id);
		$this->load->view('employee_dashboard/employee_dashboard',$data);
	}
	
}
