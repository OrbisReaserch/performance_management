<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	
	 public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        //$this->load->helper('form');
        $this->load->model('login_model');
    }
	public function index()
	{
		
		if(isset($_POST))
		{
			//$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			$this->form_validation->set_rules('email','Username','trim|required|xss_clean');
			$this->form_validation->set_rules('password','Password','trim|required|xss_clean');

			// if($this->form_validation->run() == FALSE)
			// {
			// }
			// else
			// {
				$email=trim($this->input->post('email'));
				$password=trim($this->input->post('password'));
				
				$isUserPresent=$this->login_model->checkLogin($email,$password);
				
				if($isUserPresent)
				{
					
					$session_array=array();
					$session_array = array(
							'id'	=> $isUserPresent[0]->id,
					        'username'  => $isUserPresent[0]->username,
					        'email'     => $isUserPresent[0]->email,
					        'mobile_no' => $isUserPresent[0]->mobile_no,
					        'is_admin'  => $isUserPresent[0]->is_admin,
					        'is_hr_admin' => $isUserPresent[0]->is_hr_admin,
					        'is_manager'  => $isUserPresent[0]->is_manager,
					        'is_team_leader' => $isUserPresent[0]->is_team_leader,
					        'status'     => $isUserPresent[0]->status
					    
							);
					
					$this->session->set_userdata($session_array);
					if($isUserPresent[0]->is_admin == 1 || $isUserPresent[0]->is_hr_admin == '1'){

						redirect('dashboard');

					}elseif($isUserPresent[0]->is_manager==1){

							redirect('manager_dashboard');
					}else{
							redirect('employee_dashboard');
					}
					
				}
				else
				{

					$data['error_message'] = 'Invalid Credential. Please try again.';
					$this->load->view('login/login',$data);
				}
			//}
		}
		else
		{
				if($this->session->userdata('email')!='')
				{
					redirect('login');
				}
		}
		$this->load->view('login/login');
	}

	public function get_mailid()
	{
		 $to = $_POST['email'];

	     $query = $this->db->get_where('admin',array('email'=>$to));
                if($query->num_rows()>0)
                {
                    $row=$query->result();
                    $email = $row[0]->email;
                    $password = $row[0]->pass;

                    $this->email->from('info@orbis.com', 'ORBIS');
                    $this->email->to($to); 
  
                    $this->email->subject('Recover Password');
                    $this->email->message('Your email is :'.$email.'Password is :'.$password); 
                    $this->email->send();

                    $this->email->print_debugger();
            
                  $data['error_message'] = 'Mail sent to your inbox.';
                }
                else
                {

                    $data['error_message'] = 'Entered Email is not vaild. Please try again.';
                }
                $data['include'] = 'siteadmin/login';
				$this->load->view('siteadmin/login',$data);
	}
	
}
