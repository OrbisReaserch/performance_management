<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_profile extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	 public function __construct() 
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('my_profile_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }
    public function index()
	{
        $user_id= $this->session->userdata('id');
		$data['result'] = $this->my_profile_model->getProfileDetails($user_id);
		// print_r($data['result']);exit;
       
		$this->load->view('profile/my_profile',$data);
        
	}
     public function edit_profile($id) 
    {
        // echo $id;exit;
        
        if(isset($_POST['submit']))
        {
            $username=$_POST['username'];
            $email=$_POST['email'];
            $contact_no=$_POST['contact_no'];
           // echo $email;exit;

            
            // $checkexist=$this->my_profile_model->checkProfileEmailExist($email);
            // if($checkexist==o)
            // {
            // echo "hii";exit;
                 $edit = $this->my_profile_model->edit_profile($id);
                 // echo $edit;exit;
                 if($edit)
                 {
                $this->session->set_flashdata('success', 'Profile has been updated successfully.');
                    redirect(base_url().'my_profile');
                }
                 else
                  {
                $this->session->set_flashdata('error', 'Unable to update Profile.');
               
                 redirect(base_url().'my_profile');
                  }
           
           
         // }else{
         //            $this->session->set_flashdata('error', 'Email already exist');
         //            redirect(base_url().'my_profile/');
         //    }   
        }
        else
        {
            
            $this->load->view('profile/my_profile');
        }
    }
    
	
}
