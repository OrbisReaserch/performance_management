<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	 public function __construct() 
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('department_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }
    public function index()
	{
		$data['result'] = $this->department_model->getall();
		// print_r($data['result']);exit;
       
		$this->load->view('department/department',$data);
        
	}
     public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('../login');
        }
    }
     public function add_department() 
    {
        if(isset($_POST['submit']))
        {
        	// echo "hii";
            // $this->Checklogin();exit;
            // $data['admin_section'] = 'blog';
            $department_name=$_POST['department_name'];
            $checkexist=$this->department_model->checkDepartmentNameExist($department_name);
            if($checkexist==0)
            {   

            $id = $this->department_model->add_department();

            if($id)
            {
            	echo $id;
                                     

                    $this->session->set_flashdata('success', 'Department has been added successfully.');
                    redirect(base_url().'department');

            }

            else
            {
                $this->session->set_flashdata('error', 'Unable to save department.');
                // $data['include'] = 'department/add_department';
                // $this->load->view('department/department',$data);    
                redirect(base_url().'department');
            }
         } 
          else
            {
            	// echo"hii";exit;
                $this->session->set_flashdata('error', 'Department name  already exist');
                // $data['include'] = 'department/add_department';
                // $this->load->view('department/department'); 
                 redirect(base_url().'department');
                
            }   
        }
        else
        {
            // $data['include'] = 'department/add_department';
            // $this->load->view('department/department',$data);
            redirect(base_url().'department');
        }
        
    }
    public function edit_department($id) 
    {
    	// echo "hiii";exit;
        $data['info'] = $this->department_model->getRecord($id);
        // print_r($data['info']);exit;
        if(isset($_POST['submit']))
        {
        	// echo "hello";exit;
            $id=$_POST['department_id'];
        
            // $this->Checklogin();
             $department_name=$_POST['department_name'];
             // echo $department_name;exit;
            $checkexist=$this->department_model->checkDepartmentNameExistWithId($department_name,$id);
            if($checkexist==0)
            {
            // echo "hii";exit;
                 $edit = $this->department_model->edit_department();
                 // echo $edit;exit;
                 if($edit)
                 {
                $this->session->set_flashdata('success', 'Department have been updated successfully.');
                    redirect(base_url().'department');
                }
                 else
                  {
                $this->session->set_flashdata('error', 'Unable to update Request For Quote.');
                // $data['include'] = 'siteadmin/request_for_quote/edit_requestforquote';
                // $this->load->view('backend/container', $data);
                 redirect(base_url().'department');
                  }
           
           
         }else{
                    $this->session->set_flashdata('error', 'Department name already exist');
                    redirect(base_url().'department_edit/'.$id);
            }   
        }
        else
        {
            // $data['include'] = 'department/edit_department';
            $this->load->view('department/department_edit', $data);
        }
    }
     public function delete_department($id) 
    {
        if ($this->department_model->delete($id)) 
        {
            $this->session->set_flashdata('success', 'Record has been deleted successfully.');
            redirect(base_url().'department');
        }
    }
	
}
