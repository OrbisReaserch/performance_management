<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct() 
    {
        parent::__construct();

       
        // $this->load->model('department_model');
        $this->load->model('question_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }
	public function index()
	{
		// $this->load->model('department_model'); 
		// $data['question'] = $this->question_model->getall(); 
		$data['department_result'] = $this->question_model->get_department();
		$data['question_result']=$this->question_model->getall();
        
    // print_r($data['department_result']);exit;
		$this->load->view('question/question',$data);
	}
	 // public function Checklogin() 
  //   {
  //       if ($this->session->userdata('admin_email') == '') 
  //       {
  //           redirect('../login');
  //       }
  //   }
     public function add_question() 
    {
    	// echo "hii";exit;
        if(isset($_POST['submit']))
        {
        	
            $question_name=$_POST['question'];
            $department_id=$_POST['department_id'];
            $flag=$_POST['flag'];
            // echo $question_name;exit;
            $checkexist=$this->question_model->checkQuestionNameExist($question_name);
            // echo $checkexist;exit;
            if($checkexist==0)
            {   

            $id = $this->question_model->add_question();

            if($id)
            {
            	// echo $id;exit;
                                     

                    $this->session->set_flashdata('success', 'Question has been added successfully.');
                    redirect(base_url().'question');

            }

            else
            {
                $this->session->set_flashdata('error', 'Unable to save question.');
                   
                redirect(base_url().'question');
            }
         } 
          else
            {
            	
                $this->session->set_flashdata('error', 'Question name  already exist');
                
                 redirect(base_url().'question');
                
            }   
        }
        else
        {
           
            redirect(base_url().'question');
        }
        
    }
    public function edit_question($id) 
    {
        // echo $id;exit;
        $data['info'] = $this->question_model->getRecord($id);
        $data['department_info'] = $this->question_model->get_department();
        
        // print_r($data['info']);exit;
        if(isset($_POST['submit']))
        {
            // echo "hello";exit;
            $department_id=$_POST['department_id'];
           // echo $data['info'][0]->id;exit;
            // $this->Checklogin();
             $question_name=$_POST['question'];
             $flag=$_POST['flag'];
             // echo $id."==".$data['info'][0]->department_id;exit;
            $checkexist=$this->question_model->checkQuestionNameExist($question_name);
            if($checkexist==0 ||($department_id!=$data['info'][0]->department_id) || $flag != $data['info'][0]->flag)
            {
            // echo "hii";exit;
                 $edit = $this->question_model->edit_question($id);
                 // echo $edit;exit;
                 if($edit)
                 {
                $this->session->set_flashdata('success', 'Question have been updated successfully.');
                    redirect(base_url().'question');
                }
                 else
                  {
                $this->session->set_flashdata('error', 'Unable to update Question.');
               
                 redirect(base_url().'question');
                  }
           
           
         }else{
                    $this->session->set_flashdata('error', 'Question name already exist');
                    redirect(base_url().'question/');
            }   
        }
        else
        {
            
            $this->load->view('question/question_edit', $data);
        }
    }
  //    public function get_designation_name($id) 
  //   {
  //        if(isset($_POST['dept_id']))
  //       {
  //           // echo $_POST['dept_id'];exit;
  //            $dept_id=$_POST['dept_id'];
  //            $data['designation_from_departmentID'] = $this->designation_model->getRecordForDesignationName($dept_id);
  //            // header("Content-type: application/json; charset=utf-8");
  //            echo json_encode($data['designation_from_departmentID']);
  //            // exit;
  //       }
  //   }
     public function delete_question($id) 
    {
        if ($this->question_model->delete($id)) 
        {
            $this->session->set_flashdata('success', 'Record has been deleted successfully.');
            redirect(base_url().'question');
        }
    }
	
}
