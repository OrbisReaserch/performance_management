<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Change_password extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	 public function __construct() 
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('change_password_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }
    public function index()
	{
        $user_id= $this->session->userdata('id');
		// $data['result'] = $this->my_profile_model->getProfileDetails($user_id);
		// print_r($data['result']);exit;
        // echo $user_id;exit;
       
		$this->load->view('change_password/change_password');
        
	}
  public function checkExistingPassword(){
    $ext_pwd =$_REQUEST['ext_pwd'];
    $user_id= $this->session->userdata('id');
    // echo $user_id;exit;
    if($ext_pwd != '')
    {
        $data['reports'] = $this->change_password_model->checkExistingPassword($user_id);
        // $data['reports'];exit;

        echo json_encode($data['reports']);
    }
  }
     public function edit_password($id) 
    {
        
        if(isset($_POST['submit']))
        {
            $existing_password=$_POST['existing_password'];
            $new_password=$_POST['new_password'];
            $renew_password=$_POST['renew_password'];
        // echo $renew_password;exit;
           // echo $email;exit;

            
            // $checkexist=$this->my_profile_model->checkProfileEmailExist($email);
            // if($checkexist==0 )
            // {
            // echo "hii";exit;
                 $edit = $this->change_password_model->edit_password($id);
                 // echo $edit;exit;
                 if($edit)
                 {
                $this->session->set_flashdata('success', 'Password has been updated successfully.');
                    redirect(base_url().'change_password');
                }
                 else
                  {
                $this->session->set_flashdata('error', 'Unable to update Profile.');
               
                 redirect(base_url().'change_password');
                  }
           
           
         // }else{
         //            $this->session->set_flashdata('error', 'Email already exist');
         //            redirect(base_url().'my_profile/');
         //    }   
        }
        else
        {
            
            $this->load->view('profile/my_profile',$data);
        }
    }
    
	
}
