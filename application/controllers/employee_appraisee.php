<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_appraisee extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct() 
    {
        parent::__construct();

       	$this->load->model('employee_appraisee_model');
        $this->load->model('question_model');
        $this->load->model('department_model');
        $this->load->model('designation_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }
	public function index()
	{
		$data['employee_management_details']=$this->employee_appraisee_model->get_employee_appraisee_details();
        $user_id= $this->session->userdata('id');
        // echo $user_id;exit;
        $filled_details['filled_form_details']=$this->employee_appraisee_model->filled_form_details($user_id);
        // print_r($filled_details['filled_form_details']);exit;
        $filled_details['filled_form_details_by_manager']=$this->employee_appraisee_model->filled_form_details_by_manager($user_id);
        $data['question_details']=$this->question_model->get_question_details_appraisee();
        // print_r($filled_details['filled_form_details']);exit;
        // print_r($data['question_details']);exit;
       
        if(!empty($filled_details['filled_form_details'] ))
        {
          $this->load->view('employee_appraisee/employee_appraisee', $filled_details);  
        }
        else{
		$this->load->view('employee_appraisee/employee_appraisee',$data);
    }

	}
	public function add_employee_appraisee()
	{
		// echo "hii";exit;
		// print_r($data['employee_management_details']);exit;
		$data['department_result'] = $this->department_model->getall(); 
		// print_r($data['department_result']);exit;
		// $data['result'] = $this->department_model->getall();
		$data['designation_result']=$this->designation_model->getall();

		if(isset($_POST['submit']))
        {
        	// echo "hii";echo "</br>";
           

            $job_knowledge=$_POST['customRadio4'];
            // echo $job_knowledge;echo "</br>";exit;
            $qow=$_POST['customRadio5'];
            $iam=$_POST['customRadio6'];
            $tm=$_POST['customRadio7'];
            $timemg=$_POST['customRadio8'];
            $comm=$_POST['customRadio9'];
            $proactive=$_POST['customRadio10'];
            $attendance=$_POST['customRadio11'];
            $discipline=$_POST['customRadio12'];
            $gc=$_POST['customRadio13'];
           
            $achievement=$_POST['achievement'];
            $shortfall=$_POST['shortfall'];
            $comment_by_appraisee=$_POST['comment_by_appraisee'];
            
            // echo $comment_by_appraisee;exit;
            $user_id= $this->session->userdata('id');
            // echo  $user_id;exit;
            $checkexist=$this->employee_appraisee_model->checkRatingIdExist($user_id);
            // echo $checkexist;exit;
             if($checkexist!=0)
            {  
                 $this->session->set_flashdata('error', 'You have already filled the form.');
                $this->load->view('employee_appraisee/employee_appraisee');
            }
            else{
                 // echo  $checkexist;exit;
            // 	// echo"hello";
            	$id = $this->employee_appraisee_model->add_employee_appraisee();
                // echo $id;exit;
            if($id)
            {
            	// echo $id;exit;
                                     

                    $this->session->set_flashdata('success', 'Rating has been added successfully.');
                    redirect(base_url().'employee_appraisee');

            }

            else
            {
                $this->session->set_flashdata('error', 'Unable to save department.');
                // $data['include'] = 'department/add_department';
                // $this->load->view('department/department',$data);    
                redirect(base_url().'employee_appraisee');
            }
            // } 

        }
    }
        else{
		$this->load->view('employee_appraisee/employee_appraisee',$data);
		}
	}
}
