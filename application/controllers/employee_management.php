<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_management extends CI_Controller {

	 public function __construct() 
    {
        parent::__construct();

       	$this->load->model('employee_management_model');
        $this->load->model('department_model');
        $this->load->model('designation_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }
    public function Checklogin()
    {
        if($this->session->userdata('email') ==''){

            $this->session->sess_destroy();
            redirect('welcome');
        
            }
    }
	public function index()
	{
      
            $this->Checklogin();

     $data['employee_management_details']=$this->employee_management_model->get_employee_management_details();

        $this->load->view('employee_management/employee_management',$data);
        
	}
	public function add_employee()
	{
        $this->Checklogin();
		
		// print_r($data['employee_management_details']);exit;
		$data['department_result'] = $this->department_model->getall(); 
		// print_r($data['department_result']);exit;
		// $data['result'] = $this->department_model->getall();
		$data['designation_result']=$this->designation_model->getall();
        $data['reporting_manager_details']=$this->employee_management_model->getallmanager();
        // print_r($data['reporting_manager_details']);exit;
		if(isset($_POST['submit']))
        {
        	// echo "hii";
            // $this->Checklogin();exit;
            $department_id=$_POST['department_id'];
            $designation_id=$_POST['designation_id'];
            $name_of_appraisee=$_POST['name_of_appraisee'];
            $employee_id=$_POST['employee_id'];
            $current_gross_salary=$_POST['current_gross_salary'];
            $current_net_salary=$_POST['current_net_salary'];
            $incentives=$_POST['incentives'];
            $total_leaves_taken=$_POST['total_leaves_taken'];
            $reporting_manager=$_POST['reporting_manager'];
            $authority=$_POST['authority'];
            $email=$_POST['email'];
            $contact_no=$_POST['contact_no'];
            $date_of_joining=$_POST['date_of_joining'];
            $last_appraisal_date=$_POST['last_appraisal_date'];
            $appraisal_period_from=$_POST['appraisal_period_from'];
            $appraisal_period_to=$_POST['appraisal_period_to'];

            $checkexist=$this->employee_management_model->checkEmployeeIdExist($employee_id);
             if($checkexist==0)
            {  
            	// echo"hello";
            	$id = $this->employee_management_model->add_employee();

            if($id)
            {
            	// echo $id;exit;
                                     

                    $this->session->set_flashdata('success', 'Employee Name has been added successfully.');
                    redirect(base_url().'employee_management');

            }

            else
            {
                $this->session->set_flashdata('error', 'Unable to save department.');
                // $data['include'] = 'department/add_department';
                // $this->load->view('department/department',$data);    
                redirect(base_url().'employee_management');
            }
            } 

        }
        else{
		$this->load->view('employee_management/employee_management_add',$data);
		}
	}
	public function get_designation_name($id) 
    {
         if(isset($_POST['dept_id']))
        {
            // echo $_POST['dept_id'];exit;
             $dept_id=$_POST['dept_id'];
             $data['designation_from_departmentID'] = $this->designation_model->getRecordForDesignationName($dept_id);
             // header("Content-type: application/json; charset=utf-8");
             // print_r( $data['designation_from_departmentID'] );exit;
             echo json_encode($data['designation_from_departmentID']);
             // exit;
        }
    }
    public function edit_employee($id) 
    {
        $this->Checklogin();
        // echo $id;exit;
        $data['employee_management_details'] = $this->employee_management_model->get_employee_management_detailsWithId($id);
        // print_r($data['employee_management_details']);exit;
        $data['department_result'] = $this->department_model->getall();
        $data['designation_result']=$this->designation_model->getall();
         $data['reporting_manager_details']=$this->employee_management_model->getallmanager();
        // $data['department_info'] = $this->employee_management_model->getRecordForDepartment();
        
        // print_r($data['info']);exit;
        if(isset($_POST['submit']))
        {
            $emp_id=$_POST['emp_id'];
            $user_id=$_POST['user_id'];
            $department_id=$_POST['department_id'];
            $designation_id=$_POST['designation_id'];
            $name_of_appraisee=$_POST['name_of_appraisee'];
            $employee_id=$_POST['employee_id'];
            $current_gross_salary=$_POST['current_gross_salary'];
            $current_net_salary=$_POST['current_net_salary'];
            $incentives=$_POST['incentives'];
            $total_leaves_taken=$_POST['total_leaves_taken'];
            $reporting_manager=$_POST['reporting_manager'];
            $authority=$_POST['authority'];
            $email=$_POST['email'];
            $contact_no=$_POST['contact_no'];
            $date_of_joining=$_POST['date_of_joining'];
            $last_appraisal_date=$_POST['last_appraisal_date'];
            $appraisal_period_from=$_POST['appraisal_period_from'];
            $appraisal_period_to=$_POST['appraisal_period_to'];

            // $id=$_POST['department_id'];
           // echo $data['info'][0]->id;exit;
            // $this->Checklogin();
             // $designation_name=$_POST['designation_name'];
             // echo $id."==".$data['info'][0]->department_id;exit;
            // $checkexist=$this->employee_management_model->checkEmployeeIdExist($employee_id);
            // if($checkexist==0 )
            // {
            // echo "hii";exit;
                 $edit = $this->employee_management_model->edit_employee();
                 // echo $edit;exit;
                 if($edit)
                 {
                $this->session->set_flashdata('success', 'Employee name has been updated successfully.');
                    redirect(base_url().'employee_management');
                }
                 else
                  {
                $this->session->set_flashdata('error', 'Unable to update Request For Quote.');
               
                 redirect(base_url().'designation');
                  }
           
           
         // }else{
         //            $this->session->set_flashdata('error', 'Designation name already exist');
         //            redirect(base_url().'designation/');
         //    }   
        }
        else
        {
            
            $this->load->view('employee_management/employee_management_edit',$data);
        }
    }
     public function delete_employee($id) 
    {
        $this->Checklogin();
        if ($this->employee_management_model->delete($id)) 
        {
            $this->session->set_flashdata('success', 'Record has been deleted successfully.');
            redirect(base_url().'employee_management');
        }
    }
}
