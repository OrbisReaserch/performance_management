<?php $this->load->view('header'); 
// print_r($filled_form_details_by_manager);
// $url=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
// echo $url;exit;
// $id=substr(strrchr(rtrim($url, '/'), '/'), 1);
// echo $id;
?>

      
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                     <span style="margin-left: 30px;">
                            <?php
                            if($this->session->flashdata('success'))
                            {
                                echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
                            }
                            else if($this->session->flashdata('error'))
                            {
                                echo "<font style='color:red;'>".$this->session->flashdata('error')."</font>";
                            }
                            ?>
                        </span>
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <div class="single-table ">
                                    <div class="table-responsive">
                                        <div class="col-md-12 col-md-offset-2">
                                      
                                                    <?php $rating_by_employee=0;$count_employee=0; for($j=0;$j<count($filled_form_details);$j++){
                                                        // echo $filled_form_details[$j]->rating_by_employee;echo "<br/>";
                                                        if($filled_form_details[$j]->rating_by_employee !=''){
                                                        $rating_by_employee +=$filled_form_details[$j]->rating_by_employee;
                                                        $count_employee++;
                                                    }
                                                    }
                                                    // echo $count_employee;echo "<br/>";
                                                    // echo $rating_by_employee;echo "<br/>";
                                                    $avg_by_employee=$rating_by_employee/$count_employee; 
                                                        $rating_by_manager=0;$count_manager=0;
                                                        for($ji=0;$ji<count($filled_form_details_by_manager);$ji++){
                                                            if($filled_form_details_by_manager[$ji]->rating_by_manager !=''){
                                                             $rating_by_manager +=$filled_form_details_by_manager[$ji]->rating_by_manager;
                                                             $count_manager++;
                                                            }
                                                        }
                                                        // echo $count_manager;echo "<br/>";
                                                        // echo $rating_by_manager;
                                                        $avg_by_manager=$rating_by_manager/$count_manager;
                                                        $average=($avg_by_employee+$avg_by_manager)/2;
                                                    ?>
                                                    <?php if(!is_nan($average)){?>
                                                      <table class="table table-striped ">
                                            <thead class="text-uppercase">
                                                <tr >
                                                     <td >&nbsp;</td>
                                                     <td >&nbsp;</td>
                                                    <td  scope="row" ><b>Average Rating : </b></td>
                                                     <td scope="row"><b><?php echo  round($average,2) ;?></b></td>
                                                    
                                                    <!--  <td style="text-align:right;"scope="row"><button class="btn btn-info" href="">Print</button></td> -->
                                                                                                       
                                                </tr>
                                            </thead>
                                            <tbody>
                                              <!--   <tr>
                                                    <td scope="row"><?php echo round($average,2) ;?></td>
                                                    <td>5</td>
                                                <td>3</td> -->
                                                  
                                                </tr> 
                                              
                                            </tbody>
                                        </table>
                                    <?php } ?>
                                    </div></div>
                                </div>
                                    </div>
                        <div class="row">
                             <?php if(!empty($filled_form_details_by_manager )){?>
                             <div class="col-6 mt-5">
                                <div class="card">
                                    <div class="card-body"  style="background-color:#ccc;">
                                       <h3>Manager Appraisal Form</h3>
                                       <?php
                                       for($i=0;$i<count($filled_form_details_by_manager);$i++)
                                       {
                                        // echo $i;echo"<br/>";
                                        // echo "filled_form_details".$i;echo "<br/>";
                                         // echo $filled_form_details_by_manager[$i]->question_id;echo "<br/>";
                                            if($filled_form_details_by_manager[$i]->question_id ==1)
                                            {
                                                // echo $filled_form_details_by_manager[$i]->rating_by_employee;echo "<br/>";
                                            ?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details_by_manager[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio116" name="customRadio4" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager ; } else { echo 5 ;}?>" <?php if($filled_form_details_by_manager[$i]->rating_by_manager ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio4">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio117" name="customRadio4" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 4 ;}?>" <?php if($filled_form_details_by_manager[$i]->rating_by_manager ==4) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio117">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio118" name="customRadio4" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 3 ;}?>"  <?php if($filled_form_details_by_manager[$i]->rating_by_manager ==3) { echo "checked";} ?> class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio118">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio119" name="customRadio4" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 2 ;}?>" <?php if($filled_form_details_by_manager[$i]->rating_by_manager ==2) { echo "checked";} ?> class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio119">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio120" name="customRadio4" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 1 ;}?>" <?php if($filled_form_details_by_manager[$i]->rating_by_manager ==1) { echo "checked";} ?> class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio120">1</label>
                                            </div>
                                        </div>
                                            <?php }
                                                elseif($filled_form_details_by_manager[$i]->question_id ==2)
                                            {
                                             ?>
                                            <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details_by_manager[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled  id="customRadio121" name="customRadio5" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager ; } else { echo 5 ;}?>" <?php if($filled_form_details_by_manager[$i]->rating_by_manager ==5) { echo "checked";} ?> class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio121">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio122" name="customRadio5" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 4 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==4) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio122">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio123" name="customRadio5" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 3 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio123">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio124" name="customRadio5" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 2 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio124">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio125" name="customRadio5" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 1 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio125">1</label>
                                            </div>
                                        </div>
                                            <?php }
                                            elseif($filled_form_details_by_manager[$i]->question_id ==3)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details_by_manager[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled checked id="customRadio126" name="customRadio6" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager ; } else { echo 5 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio126">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio127" name="customRadio6" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 4 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio127">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio128" name="customRadio6" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 3 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio128">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio129" name="customRadio6" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 2 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio129">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio130" name="customRadio6" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 1 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio130">1</label>
                                            </div>
                                        </div>
                                    <?php } elseif($filled_form_details_by_manager[$i]->question_id ==4)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details_by_manager[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled checked id="customRadio131" name="customRadio7" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager ; } else { echo 5 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio131">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio132" name="customRadio7" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 4 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio132">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio133" name="customRadio7" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 3 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio133">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio134" name="customRadio7" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 2 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio134">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio135" name="customRadio7" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 1 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio135">1</label>
                                            </div>
                                        </div>
                                        <?php }
                                        elseif($filled_form_details_by_manager[$i]->question_id ==5)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details_by_manager[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled checked id="customRadio136" name="customRadio8" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager ; } else { echo 5 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio136">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio137" name="customRadio8" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 4 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio137">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio138" name="customRadio8" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 3 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio138">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio139" name="customRadio8" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 2 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio139">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio140" name="customRadio8" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 1 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio140">1</label>
                                            </div>
                                        </div>

                                            <?php } elseif($filled_form_details_by_manager[$i]->question_id ==6)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details_by_manager[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled checked id="customRadio141" name="customRadio9" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager ; } else { echo 5 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio141">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio142" name="customRadio9" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 4 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio142">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio143" name="customRadio9" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 3 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio143">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio144" name="customRadio9" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 2 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio144">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio145" name="customRadio9" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 1 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio145">1</label>
                                            </div>
                                        </div>
                                        <?php } elseif($filled_form_details_by_manager[$i]->question_id ==7)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details_by_manager[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio146" name="customRadio10" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager ; } else { echo 5 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio146">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio147" name="customRadio10" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 4 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio147">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio148" name="customRadio10" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 3 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio148">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio149" name="customRadio10" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 2 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio149">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio150" name="customRadio10" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 1 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio150">1</label>
                                            </div>
                                        </div>
                                        <?php } elseif($filled_form_details_by_manager[$i]->question_id ==8)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details_by_manager[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio151" name="customRadio11" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager ; } else { echo 5 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio151">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio152" name="customRadio11" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 4 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio152">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio153" name="customRadio11" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 3 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio153">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio154" name="customRadio11" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 2 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio154">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio155" name="customRadio11" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 1 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio155">1</label>
                                            </div>
                                        </div>
                                    <?php } elseif($filled_form_details_by_manager[$i]->question_id ==9)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details_by_manager[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled checked id="customRadio156" name="customRadio12" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager ; } else { echo 5 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio156">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio157" name="customRadio12" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 4 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio157">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio158" name="customRadio12" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 3 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio158">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio159" name="customRadio12" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 2 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio159">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio160" name="customRadio12" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 1 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio160">1</label>
                                            </div>
                                        </div>
                                        <?php } elseif($filled_form_details_by_manager[$i]->question_id ==10)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details_by_manager[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled checked id="customRadio161" name="customRadio13" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager ; } else { echo 5 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio161">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio162" name="customRadio13" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 4 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio162">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio163" name="customRadio13" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 3 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio163">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio164" name="customRadio13" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 2 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio164">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" disabled id="customRadio165" name="customRadio13" value="<?php if($filled_form_details_by_manager[$i]->rating_by_manager !='') { echo $filled_form_details_by_manager[$i]->rating_by_manager; } else { echo 1 ;}?>"<?php if($filled_form_details_by_manager[$i]->rating_by_manager ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio165">1</label>
                                            </div>
                                        </div>

                                        <?php }
                                        if($filled_form_details_by_manager[$i]->question_id ==11)
                                            {
                                        ?>
                                        <div class="custom-control"><br/><br/>
                                            <label><b><?php echo $filled_form_details_by_manager[$i]->question; ?></b></label><br/>
                                            <textarea disabled rows="4" cols="50" class="form-control" name="achievement"><?php if($filled_form_details_by_manager[$i]->extra_achivement_by_manager !='') { echo $filled_form_details_by_manager[$i]->extra_achivement_by_manager; } ?></textarea>                                            
                                        </div>

                                        <?php 
                                            }
                                            elseif($filled_form_details_by_manager[$i]->question_id ==12)
                                            {
                                        ?>
                                        <div class="custom-control"><br/><br/>
                                            <label><b><?php echo $filled_form_details_by_manager[$i]->question; ?></b></label><br/>
                                            <textarea disabled rows="4" cols="50" class="form-control" name="shortfall"><?php if($filled_form_details_by_manager[$i]->shortfall_in_performance_reason !='') { echo $filled_form_details_by_manager[$i]->shortfall_in_performance_reason; } ?></textarea>                                            
                                        </div>
                                    <?php } 
                                        elseif($filled_form_details_by_manager[$i]->question_id ==13)
                                            {
                                    ?>
                                         <div class="custom-control"><br/><br/>
                                            <label><b><?php echo $filled_form_details_by_manager[$i]->question; ?></b></label><br/>
                                            <textarea disabled rows="4" cols="50" class="form-control" name="comment_by_appraisee"><?php if($filled_form_details_by_manager[$i]->comment_by_manager !='') { echo $filled_form_details_by_manager[$i]->comment_by_manager; } ?></textarea>                                            
                                        </div>
                                    <?php }
                                }
                                        
                                     ?></div>
                                        </form>
                                    </div>
                                </div>
                            <?php } else{ ?>
                                 <div class="col-6 mt-5">
                                     <!-- <div class="card"> -->
                                    <div class="card-body">
                                <div class="col-md-12"><p><b>Appraisal form not yet filled by Manager</b></p></div>
                            </div>
                        <!-- </div> -->
                        </div>
                                <?php } ?>
                            <!-- Textual inputs start -->
                            <?php if(!empty($filled_form_details)){?>
                            <div class="col-6 mt-5">
                            <form method="post" name="employee_appraisee" action="<?php echo base_url()?>employee_appraisee/add_employee_appraisee/">
                                <div class="card">
                                    <div class="card-body">
                                       <h3>Self Appraisal by the Appraisee </h3>
                                        <?php 
                                        // echo count($filled_form_details);echo"<br/>";
                                       for($i=0;$i<count($filled_form_details);$i++)
                                       {
                                        // echo $i;echo"<br/>";
                                        // echo "filled_form_details".$i;echo "<br/>";
                                         // echo $filled_form_details[$i]->question_id;echo "<br/>";
                                            if($filled_form_details[$i]->question_id ==1)
                                            {
                                                // echo $filled_form_details[$i]->rating_by_employee;echo "<br/>";
                                            ?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio"  id="customRadio116" name="customRadio4" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee ; } else { echo 5 ;}?>" <?php if($filled_form_details[$i]->rating_by_employee ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio4">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio117" name="customRadio4" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 4 ;}?>" <?php if($filled_form_details[$i]->rating_by_employee ==4) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio117">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio118" name="customRadio4" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 3 ;}?>"  <?php if($filled_form_details[$i]->rating_by_employee ==3) { echo "checked";} ?> class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio118">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio119" name="customRadio4" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 2 ;}?>" <?php if($filled_form_details[$i]->rating_by_employee ==2) { echo "checked";} ?> class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio119">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio120" name="customRadio4" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 1 ;}?>" <?php if($filled_form_details[$i]->rating_by_employee ==1) { echo "checked";} ?> class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio120">1</label>
                                            </div>
                                        </div>
                                            <?php }
                                                elseif($filled_form_details[$i]->question_id ==2)
                                            {
                                             ?>
                                            <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio"  id="customRadio121" name="customRadio5" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee ; } else { echo 5 ;}?>" <?php if($filled_form_details[$i]->rating_by_employee ==5) { echo "checked";} ?> class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio121">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio122" name="customRadio5" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 4 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==4) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio122">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio123" name="customRadio5" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 3 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio123">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio124" name="customRadio5" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 2 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio124">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio125" name="customRadio5" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 1 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio125">1</label>
                                            </div>
                                        </div>
                                            <?php }
                                            elseif($filled_form_details[$i]->question_id ==3)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" checked id="customRadio126" name="customRadio6" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee ; } else { echo 5 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio126">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio127" name="customRadio6" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 4 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio127">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio128" name="customRadio6" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 3 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio128">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio129" name="customRadio6" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 2 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio129">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio130" name="customRadio6" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 1 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio130">1</label>
                                            </div>
                                        </div>
                                    <?php } elseif($filled_form_details[$i]->question_id ==4)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" checked id="customRadio131" name="customRadio7" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee ; } else { echo 5 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio131">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio132" name="customRadio7" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 4 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio132">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio133" name="customRadio7" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 3 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio133">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio134" name="customRadio7" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 2 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio134">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio135" name="customRadio7" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 1 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio135">1</label>
                                            </div>
                                        </div>
                                        <?php }
                                        elseif($filled_form_details[$i]->question_id ==5)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" checked id="customRadio136" name="customRadio8" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee ; } else { echo 5 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio136">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio137" name="customRadio8" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 4 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio137">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio138" name="customRadio8" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 3 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio138">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio139" name="customRadio8" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 2 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio139">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio140" name="customRadio8" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 1 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio140">1</label>
                                            </div>
                                        </div>

                                            <?php } elseif($filled_form_details[$i]->question_id ==6)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" checked id="customRadio141" name="customRadio9" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee ; } else { echo 5 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio141">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio142" name="customRadio9" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 4 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio142">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio143" name="customRadio9" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 3 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio143">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio144" name="customRadio9" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 2 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio144">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio145" name="customRadio9" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 1 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio145">1</label>
                                            </div>
                                        </div>
                                        <?php } elseif($filled_form_details[$i]->question_id ==7)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" checked id="customRadio146" name="customRadio10" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee ; } else { echo 5 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio146">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio147" name="customRadio10" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 4 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio147">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio148" name="customRadio10" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 3 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio148">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio149" name="customRadio10" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 2 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio149">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio150" name="customRadio10" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 1 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio150">1</label>
                                            </div>
                                        </div>
                                        <?php } elseif($filled_form_details[$i]->question_id ==8)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" checked id="customRadio151" name="customRadio11" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee ; } else { echo 5 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio151">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio152" name="customRadio11" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 4 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio152">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio153" name="customRadio11" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 3 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio153">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio154" name="customRadio11" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 2 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio154">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio155" name="customRadio11" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 1 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio155">1</label>
                                            </div>
                                        </div>
                                    <?php } elseif($filled_form_details[$i]->question_id ==9)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" checked id="customRadio156" name="customRadio12" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee ; } else { echo 5 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio156">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio157" name="customRadio12" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 4 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio157">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio158" name="customRadio12" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 3 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio158">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio159" name="customRadio12" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 2 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio159">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio160" name="customRadio12" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 1 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio160">1</label>
                                            </div>
                                        </div>
                                        <?php } elseif($filled_form_details[$i]->question_id ==10)
                                            {?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $filled_form_details[$i]->question; ?></b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" checked id="customRadio161" name="customRadio13" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee ; } else { echo 5 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==5) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio161">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio162" name="customRadio13" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 4 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==4) { echo "checked";} ?>   class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio162">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio163" name="customRadio13" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 3 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==3) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio163">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio164" name="customRadio13" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 2 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==2) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio164">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio165" name="customRadio13" value="<?php if($filled_form_details[$i]->rating_by_employee !='') { echo $filled_form_details[$i]->rating_by_employee; } else { echo 1 ;}?>"<?php if($filled_form_details[$i]->rating_by_employee ==1) { echo "checked";} ?>  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio165">1</label>
                                            </div>
                                        </div>

                                        <?php }
                                        if($filled_form_details[$i]->question_id ==11)
                                            {
                                        ?>
                                        <div class="custom-control"><br/><br/>
                                            <label><b><?php echo $filled_form_details[$i]->question; ?></b></label><br/>
                                            <textarea rows="4" cols="50" class="form-control" name="achievement"><?php if($filled_form_details[$i]->extra_achivement_by_employee !='') { echo $filled_form_details[$i]->extra_achivement_by_employee; } ?></textarea>                                            
                                        </div>

                                        <?php 
                                            }
                                            elseif($filled_form_details[$i]->question_id ==12)
                                            {
                                        ?>
                                        <div class="custom-control"><br/><br/>
                                            <label><b><?php echo $filled_form_details[$i]->question; ?></b></label><br/>
                                            <textarea rows="4" cols="50" class="form-control" name="shortfall"><?php if($filled_form_details[$i]->shortfall_in_performance_reason !='') { echo $filled_form_details[$i]->shortfall_in_performance_reason; } ?></textarea>                                            
                                        </div>
                                    <?php } 
                                        elseif($filled_form_details[$i]->question_id ==13)
                                            {
                                    ?>
                                         <div class="custom-control"><br/><br/>
                                            <label><b><?php echo $filled_form_details[$i]->question; ?></b></label><br/>
                                            <textarea rows="4" cols="50" class="form-control" name="comment_by_appraisee"><?php if($filled_form_details[$i]->comment_by_employee !='') { echo $filled_form_details[$i]->comment_by_employee; } ?></textarea>                                            
                                        </div>
                                    <?php }
                                }
                                        
                                     ?>
                                         <div class="col-md-6 col-md-offset-2">&nbsp;
                                          <button type="submit" name="submit"class="btn btn-primary btn-block mt-4 pr-4 pl-4">Submit</button></div>
                                        </form>
                                    </div>
                                <?php }
                                    else{ ?>
                                             <div class="col-6 mt-5">
                                    <form method="post" name="employee_appraisee" id="employee_appraisee"action="<?php echo base_url()?>employee_appraisee/add_employee_appraisee">
                                <div class="card">
                                    <div class="card-body">
                                       <h3>Self Appraisal by the Appraisee </h3>
                                        <?php 
                                        // echo count($filled_form_details);echo"<br/>";
                                        $k=3;$s=2502;
                                       for($i=0;$i<count($question_details);$i++)
                                       {
                                        if($question_details[$i]->flag==0){
                                        $k++;
                                        $s++;
                                            ?>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block"><?php echo $question_details[$i]->question; ?></b>
                                            <?php for($j=5;$j>0;$j--){
                                                $s++;
                                            if($s>0)  ?>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" <?php if($j==5){ echo "checked" ;}?> id="customRadio<?php echo $s; ?>" name="customRadio<?php echo $k; ?>" value="<?php echo $j;?>"  class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio<?php echo $s; ?>"><?php echo $j; ?></label>
                                            </div>
                                        <?php } ?>
                                          
                                        </div>
                                          <?php }  elseif($question_details[$i]->flag==1) { 
                                                // echo $question_details[$i]->id;
                                            ?>

                                        <div class="custom-control"><br/><br/>
                                            <label><b><?php echo $question_details[$i]->question; ?></b></label><br/>
                                            <textarea id="<?php if(strpos($question_details[$i]->question, 'achievement')) { echo 'achievement';} elseif(strpos($question_details[$i]->question, 'performance')) { echo 'shortfall';} elseif(strpos($question_details[$i]->question, 'suggestions')){ echo 'comment_by_appraisee'; }else{ echo "fdghdgh";}?>" rows="4" cols="50" class="form-control" name="<?php if(strpos($question_details[$i]->question, 'achievement')) { echo 'achievement';} elseif(strpos($question_details[$i]->question, 'performance')) { echo 'shortfall';} elseif(strpos($question_details[$i]->question, 'suggestions')){ echo 'comment_by_appraisee'; }else{ echo "fdghdgh";}?>"></textarea>                                            
                                        </div>
                                            <?php
                                          } } ?> 
                                            

                                       
                                         <div class="col-md-6 col-md-offset-2">&nbsp;
                                          <button type="submit" id="submit" name="submit"class="btn btn-primary btn-block mt-4 pr-4 pl-4">Submit</button></div>
                                        </form>
                                    </div>

                                        <?php
                                    }
                                ?>
                                </div>
                            </div>
                      
                            <!-- Radios end -->
                    
                
     <?php $this->load->view('footer');

 ?>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
   $(document).ready(function() {

    $('#submit').click(function() {
        // $("#customRadio2502").prop("checked", true);
        // $("#customRadio2503").prop("checked", true);
        // $("#customRadio2504").prop("checked", true);
        // $("#customRadio2505").prop("checked", true);
        //  $("#customRadio2506").prop("checked", true);
        //  $("#customRadio2507").prop("checked", true);
        //  $("#customRadio2508").prop("checked", true);
        //  $("#customRadio2509").prop("checked", true); 
        // $("#customRadio2510").prop("checked", true);
        //  $("#customRadio2511").prop("checked", true);
        //  $("#customRadio2512").prop("checked", true);
        //   $("#customRadio2513").prop("checked", true);   
    // if ($('input:radio', this).is(':checked')) {
    //     // everything's fine...
    // } else {
    //     alert('Please fill all the ratings properly');
    //     return false;
    // }

    $('#achievement').prop("required", true);
    $('#shortfall').prop("required", true);
    $('#comment_by_appraisee').prop("required", true);
    if($('#achievement').val() =='')
    {
        alert('Please insert achievement');
    }
    else if($('#shortfall').val() =='')
    {
        alert('Please insert shortfall');
    }
    else if($('#comment_by_appraisee').val() =='')
    {
        alert('Please insert comment by appraisee');
    }
});


     $(".user-profile .user-name").click(function(){
    // alert('hii');
    // var tthis = $(this);
     if($(".dropdown-menu").hasClass("show")) {
    // alert(tthis);
         $(".dropdown-menu").removeClass("show");
}
else{
  $(".dropdown-menu").addClass("show");

}
  });

    // alert('hii');
  setTimeout(function(){
    $('#preloader').fadeOut('slow', function() {
      $(this).remove();
    });
   }, 2000);
});
</script>

