<?php $this->load->view('header'); ?>
        <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                           <!--  <h4 class="page-title pull-left">Dashboard</h4> -->
                            <ul class="breadcrumbs pull-left">
                                <li><a href="index.html">Home</a></li>
                                <li><span>Employee Appraisee Update</span></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                             <div class="col-6 mt-5">
                                <div class="card">
                                    <div class="card-body"  style="background-color:#ccc;">
                                       <h3>Manager Appraisal Form</h3>
                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block">Job Knoweldge- Possesses skills and knowledge to perform the job completely</b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" checked id="customRadio4" name="customRadio2" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio4">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio5" name="customRadio2" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio5">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio6" name="customRadio2" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio6">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio7" name="customRadio2" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio7">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio8" name="customRadio2" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio8">1</label>
                                            </div>
                                        </div>

                                            <div class="col-md-12">

                                            <b class="text-muted mb-3 mt-4 d-block">Quality of Work- Hold Self-accountable for the assigned responsibilities</b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" checked id="customRadio14" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio14">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio15" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio15">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio16" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio16">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio17" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio17">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio18" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio18">1</label>
                                            </div>
                                        </div>
                                    

                                         <div class="col-md-12">

                                            <b class="text-muted mb-3 mt-4 d-block">Initiative and motivation- Proficiency at improving work methods and procedures as a means to greater efficiency</b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" checked id="customRadio111" name="customRadio3" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio111">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio112" name="customRadio3" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio112">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio113" name="customRadio3" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio113">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio114" name="customRadio3" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio114">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio115" name="customRadio3" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio115">1</label>
                                            </div>
                                        </div>
                                    </div> 
                                        </form>
                                    </div>
                                </div>
                            <!-- Textual inputs start -->
                            <div class="col-6 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                       <h3>Self Appraisal by the Appraisee </h3>
                                        <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block">Job Knoweldge- Possesses skills and knowledge to perform the job completely</b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" checked id="customRadio116" name="customRadio4" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio4">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio117" name="customRadio4" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio117">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio118" name="customRadio4" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio118">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio119" name="customRadio4" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio119">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio120" name="customRadio4" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio120">1</label>
                                            </div>
                                        </div>

                                            <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block">Quality of Work- Hold Self-accountable for the assigned responsibilities</b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" checked id="customRadio121" name="customRadio5" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio121">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio122" name="customRadio5" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio122">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio123" name="customRadio5" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio123">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio124" name="customRadio5" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio124">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio125" name="customRadio5" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio125">1</label>
                                            </div>
                                        </div>

                                         <div class="col-md-12">
                                            <b class="text-muted mb-3 mt-4 d-block">initiative and motivation- Proficiency at improving work methods and procedures as a means to greater efficiency</b>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" checked id="customRadio126" name="customRadio6" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio126">5</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio127" name="customRadio6" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio127">4</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio128" name="customRadio6" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio128">3</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio129" name="customRadio6" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio129">2</label>
                                            </div>
                                             <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio130" name="customRadio6" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio130">1</label>
                                            </div>
                                        </div>
                                         <div class="col-md-6 col-md-offset-2">&nbsp;
                                          <button type="submit" class="btn btn-primary btn-block mt-4 pr-4 pl-4">Update</button></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Radios end -->
                          
        <!-- main content area end -->
      
    <!-- offset area end -->
    <!-- jquery latest version -->
    <script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
    <!-- bootstrap 4 js -->
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/metisMenu.min.js"></script>
    <script src="assets/js/jquery.slimscroll.min.js"></script>
    <script src="assets/js/jquery.slicknav.min.js"></script>

    <!-- others plugins -->
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/scripts.js"></script>
</body>

</html>
