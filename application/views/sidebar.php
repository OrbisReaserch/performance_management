<?php  $page=$this->uri->segment(1);
// echo $page;exit;
 ?>

 <!-- page container area start -->
    <div class="page-container">
<!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo">
                    <a href="index.php"><img src="<?php echo base_url()?>assets/images/icon/logo_orbis.jpg" alt="logo"></a>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">

                            <?php if ($this->session->userdata('is_admin')==1 || $this->session->userdata('is_hr_admin')==1 ){?>
                            <li <?php if($page=="dashboard"){ ?>class="active"<?php } ?> >
                                <a href="<?php echo base_url();?>dashboard" aria-expanded="true"><i class="ti-dashboard"></i><span>dashboard</span></a>
                               
                            </li>

                            <li <?php if($page=="department"){ ?>class="active"<?php } ?> >
                                <a href="<?php echo base_url();?>department" aria-expanded="true"></i><span>Department</span></a>
                            </li>

                             <li <?php if($page=="designation"){ ?>class="active"<?php } ?> >
                                <a href="<?php echo base_url();?>designation" aria-expanded="true"></i><span>Designation</span></a>
                            </li>

                             <li <?php if($page=="question"){ ?>class="active"<?php } ?> >
                                <a href="<?php echo base_url();?>question" aria-expanded="true"></i><span>Question</span></a>
                            </li>

                            <li <?php if($page=="employee_management"){ ?>class="active"<?php } ?> >
                                <a href="<?php echo base_url();?>employee_management" aria-expanded="true"></i><span>Employee Management</span></a>
                            </li>
                            <?php if ($this->session->userdata('is_admin')==1 ){?>
                             <li <?php if($page=="rate_managers"){ ?>class="active"<?php } ?> >
                                <a href="<?php echo base_url();?>rate_managers" aria-expanded="true"></i><span>Rate Managers</span></a>
                            </li>
                        <?php } ?>
                        <?php if ($this->session->userdata('is_hr_admin')==1 ){?>
                             <li <?php if($page=="self_rate"){ ?>class="active"<?php } ?> >
                                <a href="<?php echo base_url();?>self_rate" aria-expanded="true"></i><span>Self Rate</span></a>
                            </li>
                        <?php } ?>
                        <?php } 
                                    elseif ($this->session->userdata('is_manager')==1){

                        ?>
                        <li <?php if($page=="manager_dashboard"){ ?>class="active"<?php } ?> >
                                <a href="<?php echo base_url();?>manager_dashboard" aria-expanded="true"></i><span>Manager Dashboard</span></a>
                            </li>

                            <li <?php if($page=="manager_rating"){ ?>class="active"<?php } ?> >
                                <a href="<?php echo base_url();?>manager_rating" aria-expanded="true"></i><span>Rate Employees </span></a>
                            </li>
                             <li <?php if($page=="manager_self_rating"){ ?>class="active"<?php } ?> >
                                <a href="<?php echo base_url();?>manager_self_rating" aria-expanded="true"></i><span>Rate yourself</span></a>
                            </li>
                            <?php }
                            else{
                            ?>

                            <li <?php if($page=="employee_dashboard"){ ?>class="active"<?php } ?> >
                                <a href="<?php echo base_url();?>employee_dashboard" aria-expanded="true"></i><span>Employee Dashboard</span></a>
                            </li>
                             <li <?php if($page=="employee_appraisee"){ ?>class="active"<?php } ?> >
                                <a href="<?php echo base_url();?>employee_appraisee" aria-expanded="true"></i><span>Employee Appraisee</span></a>
                            </li>
                              <?php } ?>
                             
                             

                            
                           <!--  <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-layout-sidebar-left"></i><span>Sidebar
                                        Types
                                    </span></a>
                                <ul class="collapse">
                                    <li><a href="index.php">Left Sidebar</a></li>
                                    <li><a href="index3-horizontalmenu.php">Horizontal Sidebar</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-pie-chart"></i><span>Charts</span></a>
                                <ul class="collapse">
                                    <li><a href="barchart.php">bar chart</a></li>
                                    <li><a href="linechart.php">line Chart</a></li>
                                    <li><a href="piechart.php">pie chart</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-palette"></i><span>UI Features</span></a>
                                <ul class="collapse">
                                    <li><a href="accordion.php">Accordion</a></li>
                                    <li><a href="alert.php">Alert</a></li>
                                    <li><a href="badge.php">Badge</a></li>
                                    <li><a href="button.php">Button</a></li>
                                    <li><a href="button-group.php">Button Group</a></li>
                                    <li><a href="cards.php">Cards</a></li>
                                    <li><a href="dropdown.php">Dropdown</a></li>
                                    <li><a href="list-group.php">List Group</a></li>
                                    <li><a href="media-object.php">Media Object</a></li>
                                    <li><a href="modal.php">Modal</a></li>
                                    <li><a href="pagination.php">Pagination</a></li>
                                    <li><a href="popovers.php">Popover</a></li>
                                    <li><a href="progressbar.php">Progressbar</a></li>
                                    <li><a href="tab.php">Tab</a></li>
                                    <li><a href="typography.php">Typography</a></li>
                                    <li><a href="form.php">Form</a></li>
                                    <li><a href="grid.php">grid system</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-slice"></i><span>icons</span></a>
                                <ul class="collapse">
                                    <li><a href="fontawesome.php">fontawesome icons</a></li>
                                    <li><a href="themify.php">themify icons</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-table"></i>
                                    <span>Tables</span></a>
                                <ul class="collapse">
                                    <li><a href="table-basic.php">basic table</a></li>
                                    <li><a href="table-layout.php">table layout</a></li>
                                    <li><a href="datatable.php">datatable</a></li>
                                </ul>
                            </li>
                            <li><a href="maps.php"><i class="ti-map-alt"></i> <span>maps</span></a></li>
                            <li><a href="invoice.php"><i class="ti-receipt"></i> <span>Invoice Summary</span></a></li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-layers-alt"></i> <span>Pages</span></a>
                                <ul class="collapse">
                                    <li><a href="login.php">Login</a></li>
                                    <li><a href="login2.php">Login 2</a></li>
                                    <li><a href="login3.php">Login 3</a></li>
                                    <li><a href="register.php">Register</a></li>
                                    <li><a href="register2.php">Register 2</a></li>
                                    <li><a href="register3.php">Register 3</a></li>
                                    <li><a href="register4.php">Register 4</a></li>
                                    <li><a href="screenlock.php">Lock Screen</a></li>
                                    <li><a href="screenlock2.php">Lock Screen 2</a></li>
                                    <li><a href="reset-pass.php">reset password</a></li>
                                    <li><a href="pricing.php">Pricing</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-exclamation-triangle"></i>
                                    <span>Error</span></a>
                                <ul class="collapse">
                                    <li><a href="404.php">Error 404</a></li>
                                    <li><a href="403.php">Error 403</a></li>
                                    <li><a href="500.php">Error 500</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-align-left"></i> <span>Multi
                                        level menu</span></a>
                                <ul class="collapse">
                                    <li><a href="#">Item level (1)</a></li>
                                    <li><a href="#">Item level (1)</a></li>
                                    <li><a href="#" aria-expanded="true">Item level (1)</a>
                                        <ul class="collapse">
                                            <li><a href="#">Item level (2)</a></li>
                                            <li><a href="#">Item level (2)</a></li>
                                            <li><a href="#">Item level (2)</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Item level (1)</a></li>
                                </ul>
                            </li> -->
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- sidebar menu area end -->