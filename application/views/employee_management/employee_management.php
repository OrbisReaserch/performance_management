<?php  $this->load->view('header'); 
// print_r($employee_management_details);exit;
?>  
<head>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"> 
<style>
      #example_filter input {
  border-radius: 5px;
}
a{
  padding:10px;

}
/*a:hover
{
  background-color:#007bff !important;
}*/
</style></head>
            <!-- page title area start -->
           <!--  <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix"> -->
                            <!-- <h4 class="page-title pull-left">Dashboard</h4> -->
                           <!--  <ul class="breadcrumbs pull-left">
                                <li><a href="index.html">Home</a></li>
                                <li><span>Employee Management</span></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div> -->
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                      <span style="margin-left: 30px;">
                            <?php
                            if($this->session->flashdata('success'))
                            {
                                echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
                            }
                            else if($this->session->flashdata('error'))
                            {
                                echo "<font style='color:red;'>".$this->session->flashdata('error')."</font>";
                            }
                            ?>
                        </span>
                    <div class="col-lg-12 col-md-12">
                            <div class="col-md-6 ">
                                            <a href="<?php echo base_url()?>employee_management/add_employee" class="btn btn-primary mt-4 pr-4 pl-4">ADD EMPLOYEE</a></div>
                        <div class="row"><br/>
                          <div class="panel-body"><br/>

                        <div class="adv-table">
                            <table id="example" class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                    <tr>
                                        <th>Sr No</th>
                                         <th>Employee ID</th>
                                         <th>Name of Appraisee</th>
                                        <th>Department </th>
                                         <th>Designation</th>
                                         <th>Reporting Manager</th>
                                        <th style="width:180px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php

                                    for($i=0;$i<count($employee_management_details);$i++)
                                    {
                                    ?>
                                    <tr class="gradeA">
                                        <td><?php echo $i+1; ?></td>
                                         <td><?php echo $employee_management_details[$i]->employee_id; ?></td>
                                        <td><?php echo $employee_management_details[$i]->employee_name; ?></td>
                                        <td><?php echo $employee_management_details[$i]->department_name; ?></td>
                                        <td><?php echo $employee_management_details[$i]->designation_name; ?></td>
                                        <td><?php echo $employee_management_details[$i]->reporting_manager_name; ?></td>
                                        <td>
                                            <a href="<?php echo base_url();?>employee_management/edit_employee/<?php echo $employee_management_details[$i]->emp_id;?>" class="btn btn-primary btn-mini">Edit</a>
                                           
                                            <a href="<?php echo base_url();?>employee_management/delete_employee/<?php echo $employee_management_details[$i]->emp_id;?>" class="btn btn-danger btn-mini">Delete</a>
                                                                                       
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                 </div> 
            </div> 
         </div>                              
        <!-- main content area end -->
       
       
    </div>
   
    <!-- page container area end -->
   <?php $this->load->view('footer');?>
   <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js"></script>
   <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/jquery.dataTables.min.js"></script>
   <!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.jqueryui.min.js"> -->
       
<script>
 $(document).ready(function() {
    // alert('hii');         
    $('#example').DataTable({
    "bJQueryUI":true,
      "bSort":false,
      "bPaginate":true,
      "sPaginationType":"full_numbers",
       "iDisplayLength": 10
  });

} );

 </script>
   <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<script>
   $(document).ready(function() {
     $(".user-profile .user-name").click(function(){
    // alert('hii');
    // var tthis = $(this);
     if($(".dropdown-menu").hasClass("show")) {
    // alert(tthis);
         $(".dropdown-menu").removeClass("show");
}
else{
  $(".dropdown-menu").addClass("show");

}
  });
    // alert('hii');
  setTimeout(function(){
    $('#preloader').fadeOut('slow', function() {
      $(this).remove();
    });
   }, 1000);
});
</script> 