<?php  $this->load->view('header');
// print_r($this->session->userdata());exit; 
?>
<style>
    .alert{
        color:red;
    }

</style>
        
            <!-- page title area end -->

            <div class="main-content-inner">
                <div class="row">
                     <span style="margin-left: 30px;">
                            <?php
                            if($this->session->flashdata('success'))
                            {
                                echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
                            }
                            else if($this->session->flashdata('error'))
                            {
                                echo "<font style='color:red;'>".$this->session->flashdata('error')."</font>";
                            }
                            ?>
                        </span>
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                                      
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                             
                                        <form method="post" name="employee_management" id="employee_management" action="<?php echo base_url().'employee_management/add_employee/'?>">
                                       <div class="row">
                                         <div class="col-md-4">
                                                
                                                 <div class="input-group-prepend">
                                                    <div class="form-group">
                                                    <label class="col-form-label"><b>Choose department</b></label>
                                                    <select class="custom-select" name="department_id" id="department_id" ><br/>
                                                           <option selected value="">Select Department</option>
                                                            <?php
                                                            for($i=0;$i<count($department_result);$i++)
                                                            {
                                                           
                                                            ?>
                                                            <option  value="<?php echo $department_result[$i]->id;?>"><?php echo $department_result[$i]->department_name;?></option>
                                                
                                                            <?php } ?>
                                                    </select>
                                                      </div>
                                                    <div class="alert" id="employeeDepartment"></div>
                                                  </div>
                                                </div>
                                                 
                                          <div class="col-md-4">
                                                 <div class="input-group-prepend">
                                                    <div class="form-group">
                                                    <label class="col-form-label"><b>Choose Designation</b></label>
                                                    <select class="custom-select" name="designation_id" id="designation_id" ><br/>
                                                        
                                                    </select> 
                                                      </div>
                                                      <div class="alert" id="employeeDesignation"></div>
                                                  </div>

                                                </div>                                                       
                                          <div class="col-md-12">&nbsp;
                                                <div class="form-group">
                                                    <label><b>Name of Appraisee</b></label>
                                                    <input class="form-control" name="name_of_appraisee" id="name_of_appraisee"value="" placeholder="Enter Employee Name">
                                                   
                                                <div class="alert" id="employeeName"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label><b>Email ID</b></label>
                                                    <input class="form-control" id="email" name="email" placeholder="Enter Email">
                                                </div>
                                                <span class="alert" id="employeeEmail"></span>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label><b>Contact No</b></label>
                                                    <input class="form-control" name="contact_no" id="contact_no" placeholder="Enter Contact number">
                                                </div>
                                                 <span class="alert" id="employeeContact"></span>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label><b>Employee ID</b></label>
                                                    <input class="form-control" id="employee_id" name="employee_id" placeholder="Enter Employee ID">
                                                </div>
                                                 <span class="alert" id="employeeEmployeeId"></span>
                                            </div>
                                            
                                            
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Current Gross Salary</b></label>
                                                    <input class="form-control" id="current_gross_salary" name="current_gross_salary" placeholder="Enter Current Gross Salary">                                                </div>
                                                     <span class="alert" id="employeeGrossSalary"></span>
                                            </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Current Net Salary</b></label>
                                                    <input class="form-control" id="current_net_salary" name="current_net_salary" placeholder="Enter Net Salary">                                                </div>
                                                     <span class="alert" id="employeeNetSalary"></span>
                                            </div>
                                                <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Incentives</b></label>
                                                    <input class="form-control" id="incentives" name="incentives" placeholder="Enter Incentives">                                                </div>
                                                     <span class="alert" id="employeeIncentives"></span>
                                            </div> <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Total Leaves taken</b></label>
                                                    <input class="form-control" id="total_leaves_taken" name="total_leaves_taken" placeholder="Enter Total Leaves">                                                </div>
                                                     <span class="alert" id="employeeTotalLeaves"></span>
                                            </div>
                                          
                                                 <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Reporting Manager</b></label>
                                                     <select class="form-control" name="reporting_manager" id="reporting_manager">
                                                        <?php foreach($reporting_manager_details as $details){ ?>
                                                        <option value="<?php echo $details->id ?>"><?php echo $details->username; ?></option>
                                                    <?php } ?>
                                                        <!-- <option value="">Snehal</option>
                                                        <option value="">Vidisha Sonone</option>
                                                        <option value="">Hrishikesh</option> -->
                                                        
                                                    </select>
                                                    </div>
                                                     <span class="alert" id="employeeReportingManager"></span>
                                                </div>


                                                    <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Authority</b></label>
                                                     <select class="form-control" name="authority" id="authority">
                                                        <option value="" selected="">Employee</option>
                                                        <!-- <option value="Admin">Admin</option> -->
                                                        <?php if($this->session->userdata('is_admin') ==1){?>
                                                        <option value="HR">HR-Admin</option>
                                                    <?php } ?>
                                                        <option value="manager">Manager</option>
                                                        <option value="team_lead">Team Lead</option>
                                                        
                                                        
                                                    </select>
                                                    </div>
                                                    <span class="alert" id="employeeAuthority"></span>
                                                    
                                                </div>
                                                 <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Date of Joining</b></label>
                                                    <input type="date" name="date_of_joining" id="date_of_joining" class="form-control">
                                                </div>
                                                <span class="alert" id="employeeDateofJoining"></span>
                                            </div>
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Last Appraisal Date</b></label>
                                                    <input type="date" name="last_appraisal_date" id="last_appraisal_date"class="form-control">
                                                </div>
                                                <span class="alert" id="employeeLastAppraisal"></span>
                                            </div>

                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-3"><b>Appraisal Period</b></div>    
                                                <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="month" name="appraisal_period_from" id="appraisal_period_from" class="form-control">
                                                </div>
                                                <span class="alert" id="employeeAppraisalPeriodFrom"></span>
                                            </div>
                                            <div class="col-md-1"><b>to</b></div>
                                            <div class="col-md-4"> 
                                                 <input type="month" name="appraisal_period_to" id="appraisal_period_to" class="form-control">
                                            </div>
                                            <span class="alert" id="employeeAppraisalPeriodTo"></span>
                                        </div>
                                        </div>
                                         <div class="col-md-6">
                                          <button type="submit" id="submit" name="submit" class="btn btn-primary mt-4 pr-4 pl-4">Submit</button></div>
                                    </div>
                                      </form>




                                       
        <!-- main content area end -->
       
       
    </div>
    <!-- page container area end -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <script>
    
    $(document).ready(function(){
        // $('#half_description_id').show();
    // $('#full_description_id').hide();
    // alert('hii');
    $("#submit").click(function (e) {
        
        var department_id=$("#department_id").val();
        var designation_id=$("#designation_id").val();  
        var name_of_appraisee=$("#name_of_appraisee").val();
        
        var email=$("#email").val();    
        var contact_no = $("#contact_no").val();
        var employee_id = $("#employee_id").val();
        var current_gross_salary =$("#current_gross_salary").val();
        var current_net_salary =$("#current_net_salary").val();
        var incentives =$("#incentives").val();
        var total_leaves_taken =$("#total_leaves_taken").val();
        var reporting_manager =$("#reporting_manager").val();
        var authority=$("#authority").val();
        var date_of_joining=$("#date_of_joining").val();
        var last_appraisal_date=$("#last_appraisal_date").val();
        var appraisal_period_from=$("#appraisal_period_from").val();
        var appraisal_period_to=$("#appraisal_period_to").val();


        var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
        var IndNum = /^[0]?[789]\d{9}$/;
        
        $("#employeeDepartment").hide();
        $("#employeeDesignation").hide();
        $("#employeeName").hide();
        $("#employeeEmail").hide();
        $("#employeeContact").hide();
        $("#employeeEmployeeId").hide();
        $("#employeeGrossSalary").hide();
        $("#employeeNetSalary").hide();
        $("#employeeIncentives").hide();
        $("#employeeTotalLeaves").hide();
        $("#employeeReportingManager").hide();
        $("#employeeAuthority").hide();
        $("#employeeDateofJoining").hide();
        $("#employeeLastAppraisal").hide();
        $("#employeeAppraisalPeriodFrom").hide();
        $("#employeeAppraisalPeriodTo").hide();
        // $("#success_message").hide();

        if(department_id ==""){
            $("#employeeDepartment").text("Select Department ");
            $("#employeeDepartment").show();
            return false;
        }
        else if(designation_id ==""){
            $("#employeeDesignation").text("Select Designation");
            $("#employeeDesignation").show();
            return false;
        }
        else if(name_of_appraisee ==""){
            $("#employeeName").text("Enter name of appraisee");
            $("#employeeName").show();
            return false;
        }
        else if(!regexEmail.test(email)){
            $("#employeeEmail").text("Please enter valid email");
            $("#employeeEmail").show();
            return false;

        } 
        else if(!IndNum.test(contact_no)){
            $("#employeeContact").text("Please enter valid mobile number");
            $("#employeeContact").show();
            return false;

        }
         else if (employee_id == "") {
            $("#employeeEmployeeId").text("Please enter Employee Id");
            $("#employeeEmployeeId").show();
            return false;
        } else if(current_gross_salary=="") {
            $("#employeeGrossSalary").text("Please enter current gross salary");
            $("#employeeGrossSalary").show();
            return false;
        } else if(current_net_salary==""){
            $("#employeeNetSalary").text("Please enter current net salary");
            $("#employeeNetSalary").show();
            return false;   
        } 
        else if(incentives==""){
            $("#employeeIncentives").text("Please enter incentives");
            $("#employeeIncentives").show();
            return false;   
        } 
        else if(total_leaves_taken==""){
            $("#employeeTotalLeaves").text("Please enter total leaves taken");
            $("#employeeTotalLeaves").show();
            return false;   
        } 
        else if(reporting_manager==""){
            $("#employeeReportingManager").text("Please select reporting manager");
            $("#employeeReportingManager").show();
            return false;   
        } 
        // else if(authority==""){
        //     $("#employeeAuthority").text("Please select authority");
        //     $("#employeeAuthority").show();
        //     return false;   
        // } 
        else if(date_of_joining==""){
            $("#employeeDateofJoining").text("Please enter date of joining");
            $("#employeeDateofJoining").show();
            return false;   
        } 
        else if(last_appraisal_date==""){
            $("#employeeLastAppraisal").text("Please enter last appraisal date");
            $("#employeeLastAppraisal").show();
            return false;   
        } 
        else if(appraisal_period_from==""){
            $("#employeeAppraisalPeriodFrom").text("Please enter appraisal period from");
            $("#employeeAppraisalPeriodFrom").show();
            return false;   
        } 
        else if(appraisal_period_to==""){
            $("#employeeAppraisalPeriodTo").text("Please enter appraisal period to");
            $("#employeeAppraisalPeriodTo").show();
            return false;   
        } 
    });
        // else if(job_title==""){
        //     $("#whitepapererrorjob").text("Please enter jobtitle");
        //     $("#whitepapererrorjob").show();
        //     return false;   
        // } 

   $('#department_id').change(function(){
    var department_id=$('#department_id').val();
    // alert(department_id);

            $.ajax({      
            type: "POST",
            url: "<?php echo base_url().'employee_management/get_designation_name/'?>"+department_id,
            data: {
             dept_id:department_id,

            },     
            dataType:"json",
            success: function(data){ 
                // alert(data);
                // ob_clean();
                // alert(<?php //echo $info[0]->id ?>);
                jQuery('#designation_id').html('');
                
                for (i = 0; i < Object.keys(data).length; i++) {
                    // alert(i);
                    // var design_id=<?php //echo $info[0]->id; ?>;
                    // alert(design_id+'=='+data[i].id);
                $('#designation_id').append('<option  value="' +data[i].id + '">' + data[i].designation_name + '</option>');
                }
             
           } 
         });  
          
        });
});
  </script>    

    <?php $this->load->view('footer');?>
     <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
  
    
  
   </script>