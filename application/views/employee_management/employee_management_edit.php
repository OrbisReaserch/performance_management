<?php  $this->load->view('header');
print_r($employee_management_details[0]->is_hr_admin);
 ?>

        <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <!-- <h4 class="page-title pull-left">Dashboard</h4> -->
                            <ul class="breadcrumbs pull-left">
                                <li><a href="index.html">Home</a></li>
                                <li><span>Employee Details Update</span></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                       <div class="row">
                                         <div class="col-md-12">
                                                
                                                <form method="post" name="employee_management" action="<?php echo base_url().'employee_management/edit_employee/'.$employee_management_details[0]->emp_id;?>">
                                       <div class="row">
                                         <div class="col-md-4">
                                                
                                                 <div class="input-group-prepend">
                                                    <div class="form-group">
                                                    <label class="col-form-label"><b>Choose department</b></label>
                                                    <select class="custom-select" name="department_id" id="department_id"><br/>
                                                           <option  value="Select Department">Select Department</option>
                                                            <?php
                                                            for($i=0;$i<count($department_result);$i++)
                                                            {
                                                            
                                                            ?>
                                                            <option  <?php if(($department_result[$i]->id)==($employee_management_details[0]->department_id)){ echo "selected"; } ?> value="<?php echo $department_result[$i]->id;?>"><?php echo $department_result[$i]->department_name;?></option>
                                                
                                                            <?php } ?>
                                                    </select>
                                                      </div>
                                                  </div>
                                                </div>
                                                 
                                          <div class="col-md-4">
                                                 <div class="input-group-prepend">
                                                    <div class="form-group">
                                                    <label class="col-form-label"><b>Choose Designation</b></label>
                                                    <select class="custom-select" name="designation_id" id="designation_id"><br/>
                                                        
                                                    </select>
                                                      </div>
                                                  </div>

                                                </div>                                                       
                                          <div class="col-md-12">&nbsp;
                                                <div class="form-group">
                                                    <label><b>Name of Appraisee</b></label>
                                                    <input class="form-control" name="name_of_appraisee" value="<?php echo $employee_management_details[0]->employee_name; ?>" placeholder="Enter Employee Name">
                                                   
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label><b>Email ID</b></label>
                                                    <input class="form-control" name="email" value="<?php echo $employee_management_details[0]->email; ?>"placeholder="Enter Email">
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label><b>Contact No</b></label>
                                                    <input class="form-control" name="contact_no" value="<?php echo $employee_management_details[0]->mobile_no; ?>"placeholder="Enter Contact number">
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label><b>Employee ID</b></label>
                                                    <input class="form-control" name="employee_id" value="<?php echo $employee_management_details[0]->employee_id; ?>" placeholder="Enter Employee ID">
                                                </div>
                                            </div>
                                            
                                            
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Current Gross Salary</b></label>
                                                    <input class="form-control" name="current_gross_salary"value="<?php echo $employee_management_details[0]->current_gross_salary; ?>" placeholder="Enter Current Gross Salary">                                                </div>
                                            </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Current Net Salary</b></label>
                                                    <input class="form-control"  name="current_net_salary" value="<?php echo $employee_management_details[0]->current_net_salary; ?>" placeholder="Enter Net Salary">                                                </div>
                                            </div>
                                                <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Incentives</b></label>
                                                    <input class="form-control" name="incentives" value="<?php echo $employee_management_details[0]->incentive; ?>" placeholder="Enter Incentives">                                                </div>
                                            </div> <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Total Leaves taken</b></label>
                                                    <input class="form-control" name="total_leaves_taken" value="<?php echo $employee_management_details[0]->total_leave_taken; ?>" placeholder="Enter Total Leaves">                                                </div>
                                            </div>
                                          
                                                 <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Reporting Manager</b></label>
                                                     <select class="form-control" name="reporting_manager">
                                                         <?php for($i=0;$i<count($reporting_manager_details);$i++ ){ ?>
                                                        <option <?php if($reporting_manager_details[$i]->id == $employee_management_details[0]->reporting_manager_id ){ echo "selected";}?> value="<?php echo $reporting_manager_details[$i]->id ?>"><?php echo $reporting_manager_details[$i]->username; ?></option>
                                                    <?php } ?>
                                                        <!-- <option value="">Snehal</option>
                                                        <option value="">Vidisha Sonone</option>
                                                        <option value="">Hrishikesh</option> -->
                                                        
                                                    </select>
                                                    </div>
                                                </div>


                                                    <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Authority</b></label>
                                                     <select class="form-control" name="authority">
                                                          <option <?php if($employee_management_details[0]->is_hr_admin == 0 && $employee_management_details[0]->is_admin == 0 && $employee_management_details[0]->is_manager == 0){ echo "selected"; }?> value="" selected="">Employee</option>
                                                        <option <?php if($employee_management_details[0]->is_admin == 1 ){ echo "selected"; }?> value="Admin">Admin</option>
                                                        <option <?php if($employee_management_details[0]->is_hr_admin == 1 ){ echo "selected"; }?> value="HR">HR-Admin</option>
                                                        <option <?php if($employee_management_details[0]->is_manager == 1 ){ echo "selected"; }?> value="manager">Manager</option>
                                                        <option <?php if($employee_management_details[0]->is_team_leader == 1 ){ echo "selected"; }?> value="team_lead">Team Lead</option>
                                                        
                                                        
                                                    </select>
                                                    </div>
                                                    
                                                </div>
                                                 <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Date of Joining</b></label>
                                                    <input type="date" name="date_of_joining" value="<?php echo $employee_management_details[0]->date_of_joining; ?>" class="form-control">
                                                </div>
                                            </div>
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><b>Last Appraisal Date</b></label>
                                                    <input type="date" name="last_appraisal_date" value="<?php echo $employee_management_details[0]->last_appraisal_date; ?>"class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-3"><b>Appraisal Period</b></div>    
                                                <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="month" name="appraisal_period_from" 
                                                    value="<?php echo $employee_management_details[0]->appraisal_period_from; ?>"class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-1"><b>to</b></div>
                                            <div class="col-md-4"> 
                                                 <input type="month" name="appraisal_period_to" value="<?php echo $employee_management_details[0]->appraisal_period_to; ?>" class="form-control">
                                            </div>
                                        </div>
                                        <input type="hidden" name="user_id" value="<?php echo $employee_management_details[0]->user_id; ?>">
                                        <input type="hidden" name="emp_id" value="<?php echo $employee_management_details[0]->emp_id; ?>">
                                        </div>
                                         <div class="col-md-6">
                                          <button type="submit" name="submit" class="btn btn-primary mt-4 pr-4 pl-4">Update</button></div>
                                    </div>
                                      </form>




                                       
        <!-- main content area end -->
       
       
    </div>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <script>
    $(document).ready(function(){
     var dept_id=$('#department_id').val();
        if(dept_id==<?php echo $employee_management_details[0]->department_id;?>)
        {
            // alert(dept_id);
             $.ajax({      
            type: "POST",
            url: "<?php echo base_url().'employee_management/get_designation_name/'?>"+dept_id,
            data: {
             dept_id:dept_id,

            },     
            dataType:"json",
            success: function(data){ 
                // alert(data);
                 jQuery('#designation_id').html('');

                  for (j = 0; j < Object.keys(data).length; j++) {
                    // data[j].department_id;
                    $('#designation_id').append('<option  value="' +data[j].id + '">' + data[j].designation_name + '</option>');
                   
                    
                  }
                   $("#designation_id option[value=<?php echo $employee_management_details[0]->designation_id; ?>]").attr('selected', 'selected');
            }
        });
        }
      });  
   
    
    $(document).ready(function(){
       
   $('#department_id').change(function(){

    var department_id=$('#department_id').val();
    // alert(department_id);

            $.ajax({      
            type: "POST",
            url: "<?php echo base_url().'employee_management/get_designation_name/'?>"+department_id,
            data: {
             dept_id:department_id,

            },     
            dataType:"json",
            success: function(data){ 
                // alert(data);
                // ob_clean();
                // alert(<?php //echo $info[0]->id ?>);
                jQuery('#designation_id').html('');
                
                for (i = 0; i < Object.keys(data).length; i++) {
                    // alert(i);
                    // var design_id=<?php //echo $info[0]->id; ?>;
                    // alert(design_id+'=='+data[i].id);
                $('#designation_id').append('<option  value="' +data[i].id + '">' + data[i].designation_name + '</option>');
                }
             
           } 
         });  
          
        });
        
    });
   </script>

    <!-- page container area end -->
   <?php $this->load->view('footer');?>
