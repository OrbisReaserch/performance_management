<?php $this->load->view('header');?>


                    <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <!-- <h4 class="page-title pull-left">Dashboard</h4> -->
                            <ul class="breadcrumbs pull-left">
                                <li><a href="index.html">Home</a></li>
                                <li><span>Department Update</span></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        
                          <form name="department_form" method="post" action="<?php echo base_url();?>department/edit_department/<?php echo $info['id'];?>">
                                            <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Department Name</label>
                                            <input class="form-control" type="text" value="<?php echo $info['department_name'];?>" name="department_name" id="example-text-input">
                                        </div></div>
                                        <input type="hidden" name="department_id" id="department_id" value="<?php echo $info['id'];?>">
                                        <div class="col-md-6 ">
                                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4" name="submit">Update</button></div>
                                        </form>
                    </div>
            </div>
        </div>
    <!-- page container area end -->
   <?php $this->load->view('footer');

 ?>
 <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js"></script>
 <script>
   $(document).ready(function() {
     $(".user-profile .user-name").click(function(){
    // alert('hii');
    // var tthis = $(this);
     if($(".dropdown-menu").hasClass("show")) {
    // alert(tthis);
         $(".dropdown-menu").removeClass("show");
}
else{
  $(".dropdown-menu").addClass("show");

}
  });
    // alert('hii');
  setTimeout(function(){
    $('#preloader').fadeOut('slow', function() {
      $(this).remove();
    });
   }, 1000);
});
</script> 
