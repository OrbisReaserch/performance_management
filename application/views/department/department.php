<?php $this->load->view('header');?>
<head>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"> 
<style>
      #example_filter input {
  border-radius: 5px;
}
a{
  padding:10px;

}
/*a:hover
{
  background-color:#007bff !important;
}*/
</style></head>
        
        <!-- page title area start -->
           
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                     <span style="margin-left: 30px;">
                            <?php
                            if($this->session->flashdata('success'))
                            {
                                echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
                            }
                            else if($this->session->flashdata('error'))
                            {
                                echo "<font style='color:red;'>".$this->session->flashdata('error')."</font>";
                            }
                            ?>
                        </span>
                    <div class="col-lg-12 col-md-12">
                        
                          <form name="department_form" action="<?php echo base_url();?>department/add_department" method="post">
                                            <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Department Name</label>
                                            <input class="form-control" required type="text" placeholder="Enter Department" value="" name="department_name" id="example-text-input" >
                                        </div>
                                    </div>
                                        <div class="col-md-6 ">
                                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4" name="submit">ADD</button></div>
                                        </form>
                            </div>
                        </div>
                        </div>
                <div class="panel-body">

                        <div class="adv-table">
                            <table id="example" class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                    <tr>
                                        <th>Sr No</th>
                                        <th>Department Name</th>
                                         <th>Date</th>
                                        <th style="width:180px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php

                                    for($i=0;$i<count($result);$i++)
                                    {
                                    ?>
                                    <tr class="gradeA">
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $result[$i]->department_name;?></td>
                                        <td><?php echo $result[$i]->last_updated_on;?></td>
                                        <td>
                                            <a href="<?php echo base_url();?>department/edit_department/<?php echo $result[$i]->id;?>" class="btn btn-primary btn-mini">Edit</a>
                                           
                                            <a href="<?php echo base_url();?>department/delete_department/<?php echo $result[$i]->id;?>" class="btn btn-danger btn-mini">Delete</a>
                                                                                       
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
    <!-- page container area end -->
   <?php $this->load->view('footer');?>
    <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js"></script>
   <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/jquery.dataTables.min.js"></script>
   <script>
 $(document).ready(function() {
    // alert('hii');         
    $('#example').DataTable({
    "bJQueryUI":true,
      "bSort":false,
      "bPaginate":true,
      "sPaginationType":"full_numbers",
       "iDisplayLength": 10
  });

} );

 </script>
 <script>
   $(document).ready(function() {
     $(".user-profile .user-name").click(function(){
    // alert('hii');
    // var tthis = $(this);
     if($(".dropdown-menu").hasClass("show")) {
    // alert(tthis);
         $(".dropdown-menu").removeClass("show");
}
else{
  $(".dropdown-menu").addClass("show");

}
  });
    // alert('hii');
  setTimeout(function(){
    $('#preloader').fadeOut('slow', function() {
      $(this).remove();
    });
   }, 1000);
});
</script> 