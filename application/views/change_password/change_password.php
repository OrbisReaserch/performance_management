<?php $this->load->view('header');
// print_r($result);
 $user_id= $this->session->userdata('id');
?>


          
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                      <span style="margin-left: 30px;">
                            <?php
                            if($this->session->flashdata('success'))
                            {
                                echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
                            }
                            else if($this->session->flashdata('error'))
                            {
                                echo "<font style='color:red;'>".$this->session->flashdata('error')."</font>";
                            }
                            ?>
                        </span>
                    <div class="col-lg-12 col-md-12">
                        
                         <div class="container-contact100">
        <div class="wrap-contact100">
            <form class="contact100-form validate-form" method="post" action="<?php echo base_url()?>change_password/edit_password/<?php echo $user_id; ?>">
                                                                   
              <div class="col-md-6">&nbsp;
                    <div class="form-group">
                        <label><b>Old Password</b></label>
                        <div id="existing_pwd_error" class="alert alert-danger"></div>
                        <div id="existing_pwd_success" class="alert alert-success"></div>
                        <input class="form-control" required id="existing_password" type="password" name="existing_password" value="" placeholder="">
                       
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="form-group">
                        <label><b>Enter New Password</b></label>
                        <input class="form-control" required id="new_password"  type="password" value="" name="new_password" >
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="form-group">
                        <label><b>Re-enter New Password</b></label>
                        <div id="entered_new_password" required class="alert alert-danger"></div>
                        <div id="entered_new_password_success" required class="alert alert-success"></div>
                        <input class="form-control" name="renew_password"  id="renew_password" type="password" value=""  placeholder="">
                    </div>
                </div>
              

              <div class="col-md-6">
                 <button type="submit" name="submit" class="btn btn-primary mt-4 pr-4 pl-4">Submit</button></div>
            </form>
        </div>
    </div>
                    </div>
            </div>
        </div>
    <!-- page container area end -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() 
        {
            // alert('hii');
            // if($("#existing_pwd_error").text() =='')
            $("button").hide();
            if ($('#existing_pwd_error').is(':empty'))
            {
                $("#existing_pwd_error").hide();
                $("#existing_pwd_success").hide();
            }

            
        $("#existing_password").keyup(function() {
                // $("#searchlist").html('');
                var ext_pwd=  $("#existing_password").val();
                // alert(ext_pwd);
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url() ?>change_password/checkExistingPassword',
                    dataType:'json',
                    data: {
                        'ext_pwd':ext_pwd
                    },
                    success: function(data) {
                        // alert(data[0].password);
                       
                        if(data[0].password == ext_pwd)
                        {
                            // alert('password match');
                             $("#existing_pwd_success").show();
                             $("#existing_pwd_error").hide();
                             $("#existing_pwd_success").text('Password matched');

                        }
                        else{
                             $("#existing_pwd_error").show();
                             $("#existing_pwd_success").hide();
                            $("#existing_pwd_error").text('Password does not match with your old password');
                        }

                        // var content='';
                        // $.each( data.reports, function( index, value ) {
                        //     content+='<li class="autolist" title="'+value.report_name+'" data-pageurl="'+value.report_key_url+'">'+value.report_name+ '</li>'
                        // });
                       
                    }
                    });
            });
         if ($('#entered_new_password').is(':empty'))
            {
                $("#entered_new_password").hide();
                $("#entered_new_password_success").hide();
            }
            
               
            
         $("#renew_password").keyup(function() {
                // $("#searchlist").html('');
                var new_pwd=  $("#new_password").val();
                var renew_pwd=$("#renew_password").val();
                // alert(new_pwd);
               
                        // alert(data[0].password);
            if(new_pwd == renew_pwd){
                // alert('hii');
                 $("#entered_new_password").hide();
                 $("#entered_new_password_success").show();
                $("#entered_new_password_success").text('Password matches with new password');
                $("button").show();
                
            }
            else
            {
                $("#entered_new_password_success").hide();
                 $("#entered_new_password").show();
                $("#entered_new_password").text('Password does not match with new password');
            }

                       
                       
                    
                  
            });
    });

    </script>
   <?php $this->load->view('footer');

 ?>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
   $(document).ready(function() {
     $(".user-profile .user-name").click(function(){
    // alert('hii');
    // var tthis = $(this);
     if($(".dropdown-menu").hasClass("show")) {
    // alert(tthis);
         $(".dropdown-menu").removeClass("show");
}
else{
  $(".dropdown-menu").addClass("show");

}
  });

    // alert('hii');
  setTimeout(function(){
    $('#preloader').fadeOut('slow', function() {
      $(this).remove();
    });
   }, 4000);
});
</script>

