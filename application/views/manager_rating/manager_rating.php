<?php $this->load->view('header'); 
// print_r($employee_rating_details);
?>
<head>
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.material.min.css">
   <!-- <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/css/jquery.dataTables_themeroller.css"> -->
   <style>
   #example_filter input {
  border-radius: 5px;
}
a{
  padding:10px;

}
</style>
</head>
     
     
        <!-- <div class="row"> -->
             <span style="margin-left: 30px;">
                            <?php
                            if($this->session->flashdata('success'))
                            {
                                echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
                            }
                            else if($this->session->flashdata('error'))
                            {
                                echo "<font style='color:red;'>".$this->session->flashdata('error')."</font>";
                            }
                            ?>
                        </span><br/>
     <table id="example" class="table table-striped table-bordered" style="width:97%"><br/>
        <thead>
            <tr>
                <th>Sr No</th>
                <th>Emp ID</th>
                <th>Name</th>
                <th>Department Name</th>
                <th>Designation Name</th>
                <!-- <th>Manager Name</th> -->
                <!-- <th> date</th> -->
                <th>Action</th>
            </tr>
        </thead>
     <tbody>
                                    <?php
                                    // echo count($employee_rating_details);
                                    for($i=0;$i<count($employee_rating_details);$i++)
                                    {
                                    ?>
                                    <tr class="gradeA">
                                        <td><?php echo $i+1;?></td>
                                         <td><?php echo $employee_rating_details[$i]->employee_id;?></td>
                                        <td><?php echo $employee_rating_details[$i]->employee_name;?></td>
                                        <td><?php echo $employee_rating_details[$i]->department_name;?></td>
                                        <td><?php echo $employee_rating_details[$i]->designation_name;?></td>
                                        <!-- <td><?php echo $employee_rating_details[$i]->reporting_manager_name;?></td> -->
                                        <td>
                                            <a href="<?php echo base_url();?>manager_rating/edit_manager_rating/<?php echo $employee_rating_details[$i]->id;?>" class="btn btn-primary btn-mini">Edit</a>
                                           
                                            <!-- <a href="<?php echo base_url();?>manager_rating/edit_manager_rating/<?php echo $employee_rating_details[$i]->id;?>" class="btn btn-danger btn-mini">Delete</a> -->
                                                                                       
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    
                                    
                                </tbody>
        <tfoot>
            <tr>
                <th>Sr No</th>
                <th>Name</th>
                <th>Emp ID</th>
                <th>Department Name</th>
                <th>Designation Name</th>
                <!-- <th> date</th> -->
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
<!-- </div> -->
            <!-- page title area end -->
           
<script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js"></script>
   <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/jquery.dataTables.min.js"></script>
<script>
 $(document).ready(function() {
    // alert('hii');
     $('#example').DataTable({
    "bJQueryUI":true,
      "bSort":false,
      "bPaginate":true,
      "sPaginationType":"full_numbers",
       "iDisplayLength": 10
  });
} );

 </script>
                            <!-- Radios end -->
<?php $this->load->view('footer'); ?>
