<?php $this->load->view('header'); ?>
       

        <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <!-- <h4 class="page-title pull-left">Dashboard</h4> -->
                            <ul class="breadcrumbs pull-left">
                                <li><a href="index.html">Home</a></li>
                                <li><span>Designation Update</span></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        
                          <form name="department_form" method="post"  action="<?php echo base_url();?>designation/edit_designation/<?php echo $info[0]->id;?>">
                                            <div class="col-md-6">
                                         <div class="input-group-prepend" >
                                              <div class="form-group">
                                            <label class="col-form-label"><b>Choose department</b></label>
                                                      <select class="custom-select" name="department_id" id="department_id"><br/>
                                                           <option  value="Select Department">Select Department</option>
                                                            <?php
                                                            for($i=0;$i<count($department_info);$i++)
                                                            {                 
                                                            ?>
                                                            <option  
                                                            value="<?php echo $department_info[$i]->id;?>" <?php if(($department_info[$i]->id)==($info[0]->department_id)){ 
                                                                            echo "selected"; 
                                                                                 } ?> ><?php echo $department_info[$i]->department_name;?></option>
                                                
                                                            <?php } ?>
                                                    </select>
                                                </div>
                                                </div></div>
                                                
                                                 <div class="col-md-6">
                                                    
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Designation </label>
                                            <input class="form-control" name="designation_name"type="text" value="<?php echo  $info[0]->designation_name;?>" id="example-text-input">
                                        </div></div>
                                         <input type="hidden" name="designation_id" id="designation_id" value="<?php echo $info[0]->id;?>">
                                        <div class="col-md-6 ">
                                            <button type="submit" name="submit"class="btn btn-primary mt-4 pr-4 pl-4">Update</button></div>
                                        </form>
    </div>
</div>
</div>
 
    <!-- page container area end -->
   <?php
   $this->load->view('footer');
   ?>
   <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js"></script>
 <script>
   $(document).ready(function() {
     $(".user-profile .user-name").click(function(){
    // alert('hii');
    // var tthis = $(this);
     if($(".dropdown-menu").hasClass("show")) {
    // alert(tthis);
         $(".dropdown-menu").removeClass("show");
}
else{
  $(".dropdown-menu").addClass("show");

}
  });
    // alert('hii');
  setTimeout(function(){
    $('#preloader').fadeOut('slow', function() {
      $(this).remove();
    });
   }, 1000);
});
</script> 
  
